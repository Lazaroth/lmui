# LMUI 0.4 (WIP) #

Lazaroth's Modern UI for the [XL Engine](http://xlengine.com).

### About ###

A replacement for the original [Daggerfall](http://www.elderscrolls.com/daggerfall/) UI. Meant for use with [Awesomium](http://www.awesomium.com/) and built using jQuery. Still very much a WIP.

Recommended browser for testing is [Chrome](https://www.google.com/chrome/browser/) since both are built on [Chromium](http://www.chromium.org). May not work if run locally.

### Features ###

* Dynamic Plug-And-Play Window Creation
* Themes (currently 2 are available, Daggerfall and Skyrim). Easily create your own with CSS and JSON
* Use Workspaces to place windows on and bind a button to that workspace. Want the map and journal window on the same key, no problem!
* Sticky Windows
* Sort the inventory however you like and choose between list or grid layout
* Option to use a simple 3d engine to walk around and use the UI in

### How to Contribute ###

* Code the UI. Lots of missing features still
* Create a UI Theme. A guide can be found in *Theme Creation Guide.txt*
* Finding bugs; though not in things listed as being worked on or unimplemented features

### Contact ###

* XL Engine Forum PM
* Bitbucket Message