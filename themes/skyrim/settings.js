{
	"settings" :
	{
		"margins"		: 100,
		"fxshow"		: false,
		"workspaces"	: [	{
								"number"	: 1,
								"name"		: "Inventory",
								"button"	: 9,
								"windows"	:
								[
									"inventory",
									"apparel",
									"player",
									"map",
									"spells"
								]
							},
							{
								"number"	: 2,
								"name"		: "Journal",
								"button"	: 74,
								"windows"	:
								[
									"journal"
								]
							}
						]
	},
	"windows" :
	{
		"opacity"		: 0.9
	},
	"player" :
	{
		"x"				: 20,
		"y"				: 20,
		"width"			: 230,
		"height"		: 540
	},
	"inventory" :
	{
		"x"				: 270,
		"y"				: 20,
		"width"			: 430,
		"height"		: 540
	},
	"map" :
	{
		"x"				: 720,
		"y"				: 20,
		"width"			: 320,
		"height"		: 180
	},
	"apparel" :
	{
		"x"				: 720,
		"y"				: 225,
		"width"			: 320,
		"height"		: 335
	},
	"spells" :
	{
		"x"				: 1060,
		"y"				: 20,
		"width"			: 200,
		"height"		: 540
	},
	"journal" :
	{
		"x"				: 350,
		"y"				: 20,
		"width"			: 580,
		"height"		: 520
	}
}