themeList = [
	{	"name"		: "Daggerfall",
		"folder"	: "daggerfall",
		"author"	: "Lazaroth",
		"info"		: "Theme inspired by Daggerfall",
		"version"	: 0.2
	},
	{	"name"		: "Skyrim",
		"folder"	: "skyrim",
		"author"	: "Lazaroth",
		"info"		: "Theme inspired by Skyrim",
		"version"	: 0.2
	}
]