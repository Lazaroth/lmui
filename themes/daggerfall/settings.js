{
	"settings" :
	{
		"margins"		: 50,
		"fxshow"		: false,
		"workspaces"	: [	{
								"number"	: 1,
								"name"		: "Inventory",
								"button"	: 9,
								"windows"	:
								[
									"inventory",
									"spells"
								]
							},
							{
								"number"	: 2,
								"name"		: "Player",
								"button"	: 67,
								"windows"	:
								[
									"player",
									"apparel"
								]
							},
							{
								"number"	: 3,
								"name"		: "Map",
								"button"	: 77,
								"windows"	:
								[
									"map"
								]
							},
							{
								"number"	: 4,
								"name"		: "Journal",
								"button"	: 74,
								"windows"	:
								[
									"journal"
								]
							}
						]
	},
	"windows" :
	{
		"opacity"		: 1
	},
	"inventory" :
	{
		"x"				: 260,
		"y"				: 0,
		"width"			: 500,
		"height"		: 500
	},
	"spells" :
	{
		"x"				: 760,
		"y"				: 0,
		"width"			: 260,
		"height"		: 500
	},
	"map" :
	{
		"x"				: 275,
		"y"				: 0,
		"width"			: 730,
		"height"		: 400
	},
	"player" :
	{
		"x"				: 340,
		"y"				: 0,
		"width"			: 300,
		"height"		: 500
	},
	"apparel" :
	{
		"x"				: 640,
		"y"				: 0,
		"width"			: 320,
		"height"		: 200
	},
	"journal" :
	{
		"x"				: 350,
		"y"				: 20,
		"width"			: 580,
		"height"		: 520
	}
}