Themes = {
	selectTheme: function(theme, selecttheme, callback)
	{
		Themes.loadStyle(theme);
		Themes.loadTheme(theme, selecttheme, callback);
	},
	loadStyle: function(theme, callback)
	{
		if($('#linktheme').length > 0) // on change
		{
			JUtils.saveSettings('settings', {'theme': theme});
			$('#linktheme')
				.load(function()
				{
					if(typeof callback === 'function')
					{
						callback.call(this);
					}
				})
				.attr('href','themes/'+theme+'/style.css');
		}
		else // on first load
		{
			$('head').append('<link type="text/css" rel="stylesheet" id="linktheme" href="themes/'+theme+'/style.css"/>');
		}

		if(typeof callback === 'function')
		{
			callback.call(this);
		}
	},
	loadTheme: function(theme, selecttheme, callback)
	{
		$.ajax(
		{
			url: 'themes/'+theme+'/settings.js',
			cache: false,
			dataType: 'json',
			mimeType: 'application/json',
			success: function(data)
			{
				Themes.applyTheme(data, selecttheme, callback);
			},
			error: function()
			{
				Themes.applyTheme(Defaults.WindowSettings(), selecttheme, callback);
			}
		});
	},
	applyTheme: function(data, selecttheme, callback)
	{
		var settings = {};
		var css = {'css' : {}};
		var it = 0;

		$.each(data, function(key, val)
		{
			var old = JUtils.loadSettings(key);

			if(typeof val["x"] != 'undefined')
			{
				val["x"] = UI.x(val["x"]);
				$('#'+key).css('left', val["x"]);
			}

			if(typeof val["y"] != 'undefined')
			{
				val["y"] = UI.x(val["y"]);
				$('#'+key).css('top', val["y"]);
			}

			if(typeof val["width"] != 'undefined')
			{
				val["width"] = UI.x(val["width"]);
				$('#'+key).css('width', val["width"]);
			}

			if(typeof val["height"] != 'undefined')
			{
				val["height"] = UI.x(val["height"]);
				$('#'+key).css('height', val["height"]);
			}

			if(key == 'windows')
			{
				css = {'css' : val};
			}

			if(key == 'settings')
			{
				val['workspaces'] = UI.popuplateWorkspaces(val['workspaces']);
				settings = $.extend(Defaults.WindowSettings()['settings'], old, val);
			}

			if(selecttheme === true) // overwrite old positions/sizes/workspaces
			{
				var o = $.extend(old, val);
				JUtils.saveSettings(key, o);
			}
			else // remember old window positions/sizes/workspaces
			{
				JUtils.saveSettings(key, $.extend(val, old));
			}

			it++;
		});

		JUtils.saveSettings('settings', css);

		if(typeof callback === 'function')
		{
			callback.call(this, settings);
		}
	}
};