FX = {
	show : function(type)
	{
		var o = JUtils.loadSettings('settings');

		switch(type)
		{
			case 'night':
				$.firefly();
			break;

			case 'snow':
				$(o.attachto).snowfall();
			break;

			case 'rain':
				if($('#bgimage').length > 0)
				{
					RUNRAIN = true;
					$(o.attachto).append('<canvas id="raincanvas" style="position: absolute; top: 0px; left: 0px; z-index: 30;"></canvas>');
					RAINENGINE = new RainyDay('raincanvas','bgimage', $(o.attachto).width(), $(o.attachto).height(), 1, 1);
					RAINENGINE.gravity = RAINENGINE.GRAVITY_NON_LINEAR;
					RAINENGINE.trail = RAINENGINE.TRAIL_DROPS;
					RAINENGINE.rain([ RAINENGINE.preset(7, 3, 0.88), RAINENGINE.preset(8, 5, 0.9), RAINENGINE.preset(9, 2, 1) ], 100);
				}
			break;
		}
	},
	hide : function(type, attachto)
	{
		var o = JUtils.loadSettings('settings');

		switch(type)
		{
			case 'night':
				$.firefly({destroy: true});
			break;

			case 'snow':
				$(o.attachto).snowfall('clear');
			break;

			case 'rain':
				$('#raincanvas').remove();
				if(typeof RUNRAIN !== 'undefined') { RUNRAIN = undefined; }
			break;
		}
	}
};