Calc = {
	weaponWeight: function(weapon, material)
	{
		var baseweight	= DB.items().weapon[weapon][1];
		var weightmod	= DB.material()[material][0];
		return Math.round(baseweight * weightmod * 100) / 100;
	},
	weaponValue: function(weapon, material)
	{
		var basevalue	= DB.items().weapon[weapon][2];
		var valuemod	= DB.material()[material][1];
		return basevalue * valuemod;
	},
	weaponDamage: function(weapon, material)
	{
		var minbase = DB.items().weapon[weapon][3];
		var maxbase = DB.items().weapon[weapon][4];
		var damagemod	= DB.material()[material][2];
		return minbase*damagemod + '-' + maxbase*damagemod;
	},
	armorWeight: function(armor, material)
	{
		var baseweight	= DB.items().armor[armor][1];
		var weightmod	= DB.material()[material][0];
		return Math.round(baseweight * weightmod * 100) / 100;
	},
	armorValue: function(armor, material)
	{
		var basevalue	= DB.items().armor[armor][2];
		var valuemod	= DB.material()[material][1];
		return basevalue * valuemod;
	}
}