Defaults = {
	xConst : 1280,
	yConst : 720,
	Workspaces: 5,
	mapinfo: [5000, 2500, 'images/map.jpg'],
	World : {
		size		: 32,
		current		: "snow",
		active		: true,
		weapon		: "fists",
		paused		: false,
		settings	: {	"night" :	{	lightRange	: 7,
										fadeTo		: "#1e1010",
										raining		: false
									},
						"rain" :	{	lightRange	: 9,
										fadeTo		: "#4b595d",
										raining		: true
									},
						"snow" :	{	lightRange	: 20,
										fadeTo		: "#807e7e",
										raining		: false
									}
					  }
	},
	WindowSettings: function()
	{
		var settings =
		{
			"settings" :
			{
				"fxtimer"		: 200,			// the time effects take in ms (fadeIn, fadeOut etc)
				"attachto"		: "body",		// where to attach all content
				"scene"			: "night",		// the default scene to use
				"theme"			: "skyrim",	// the default theme to use
				"fxshow"		: false,		// show inventory effects
				"world"			: false,		// render 3d world
				"scale"			: 100,			// the default UI scale
				"margins"		: 100,			// the default UI margins
				"mapfilter"		: [],			// array of hidden map layers
				"sticky"		: [],
				"workspaces"	: [	{
										"number"	: 1,
										"name"		: "Inventory",
										"button"	: 9,
										"windows"	:
										[
											"inventory",
											"spells"
										]
									},
									{
										"number"	: 2,
										"name"		: "Player",
										"button"	: 67,
										"windows"	:
										[
											"player",
											"apparel"
										]
									},
									{
										"number"	: 3,
										"name"		: "Map",
										"button"	: 77,
										"windows"	:
										[
											"map"
										]
									},
									{
										"number"	: 4,
										"name"		: "Journal",
										"button"	: 74,
										"windows"	:
										[
											"journal"
										]
									}
								]
			},
			"windows" :
			{
				"opacity"		: 1
			},
			"inventory" :
			{
				"x"				: 260,
				"y"				: 0,
				"width"			: 500,
				"height"		: 500
			},
			"spells" :
			{
				"x"				: 760,
				"y"				: 0,
				"width"			: 260,
				"height"		: 500
			},
			"map" :
			{
				"x"				: 390,
				"y"				: 0,
				"width"			: 500,
				"height"		: 500
			},
			"player" :
			{
				"x"				: 340,
				"y"				: 0,
				"width"			: 300,
				"height"		: 500
			},
			"apparel" :
			{
				"x"				: 640,
				"y"				: 0,
				"width"			: 320,
				"height"		: 200
			},
			"journal" :
			{
				"x"				: 350,
				"y"				: 20,
				"width"			: 580,
				"height"		: 520
			},
			"changelog" :
			{
				"x"				: 475,
				"y"				: 20,
				"width"			: 330,
				"height"		: 640
			}
		}

		settings['settings']['workspaces'] = UI.popuplateWorkspaces(settings['settings']['workspaces']);
		return settings;
	},
	mapLayerTypes: {
		"town" : {
			icon1:		'town.png',
			icon2:		'town_disabled.png',
			visible:	true,
			features:	[]
		},
		"home" : {
			icon1:		'home.png',
			icon2:		'home_disabled.png',
			visible:	true,
			features:	[]
		},
		"dungeon" : {
			icon1:		'dungeon.png',
			icon2:		'dungeon_disabled.png',
			visible:	true,
			features:	[]
		},
		"temple" : {
			icon1:		'temple.png',
			icon2:		'temple_disabled.png',
			visible:	true,
			features:	[]
		}
	},
	windowList: function()
	{
		// [ Filename/Internal Name, Header, Create on Load ]

		var list = [	{	name:	"alchemy"
						},
						{	name:	"apparel",
							header:	"Apparel",
							create:	true
						},
						{	name:	"banking"
						},
						{	name:	"barter"
						},
						{	name:	"changelog",
							header:	"Changelog"
						},
						{	name:	"chargen"
						},
						{	name:	"daedrasummon",
							header:	"Summon	Daedra"
						},
						{	name:	"enchanting",
							header:	"Enchant"
						},
						{	name:	"gameoptions",
							header:	"Options"
						},
						{	name:	"inventory",
							header:	"Inventory",
							create:	true
						},
						{	name:	"itemidentification",
							header:	"Identify"
						},
						{	name:	"jail"
						},
						{	name:	"journal",
							header:	"Journal",
							create:	true
						},
						{	name:	"letter"
						},
						{	name:	"load",
							header:	"Load Game"
						},
						{	name:	"map",
							header:	"Map",
							create:	true
						},
						{	name:	"npcspeak"
						},
						{	name:	"player",
							create:	true
						},
						{	name:	"potionmaker",
							header:	"Potion	Maker"
						},
						{	name:	"save",
							header:	"Save Game"
						},
						{	name:	"settings"
						},
						{	name:	"spellmaker",
							header:	"Spell	Maker"
						},
						{	name:	"spells",
							header:	"Spells",
							create:	true
						},
						{	name:	"spymaster"
						},
						{	name:	"teleport"
						},
						{	name:	"training"
						}
		];

		return list;
	},
	unknownWindow: function()
	{
		var settings =	{	type:		'Unknown Window',
							x:			UI.x(0),
							y:			UI.y(0),
							alignX:		'left',
							alignY:		'top',
							width:		UI.width(500),
							height:		UI.height(500),
							workspace:	Defaults.Workspaces
						};

		return settings;
	},
};