JUtils = {
	saveSettings: function(type, settings)
	{
		var oldsettings = JUtils.loadSettings(type);
		var newsettings = $.extend(oldsettings, settings);
		$.cookie(type, JSON.stringify(newsettings));
	},
	loadSettings: function(type)
	{
		return ($.cookie(type)) ? JSON.parse($.cookie(type)) : {};
	},
	endsWith: function(str, suffix)
	{
		return str.toString().indexOf(suffix, str.toString().length - suffix.length) !== -1;
	},
	ranNum: function(from, to)
	{
		return Math.floor(Math.random()*(to-from+1)+from);
	},
	ranKey: function(obj)
	{
		var ret;
		var c = 0;

		for(var key in obj)
		{
			if(Math.random() < 1/++c)
			{
				ret = key;
			}
		}

		return ret;
	},
	rotateCompass: function()
	{
		var ang = $('#compassneedle').getRotateAngle();
		var settings = JUtils.loadSettings('settings');

		if((ang == 0 || ang == 360) && settings.world == false)
		{
			$('#compassneedle').rotate({
				angle: 0,
				animateTo: 360,
				duration: 1500
			});
		}
	},
	changeToolTip: function(att, str)
	{
		var settings = JUtils.tooltipSettings;
		settings['content'] = str;
		att.tooltip(settings).mouseout().mouseover();
	},
	tooltipSettings:
	{
		track: true,
		show: false,
		hide: false,
		position:
		{
			my: "left+25 top",
			at: "right center"
		},
		content: function()
		{
			return $(this).attr('title');
		}
	},
	capFirst: function(string)
	{
		return string.charAt(0).toUpperCase() + string.slice(1);
	},
	getElement: function(elem)
	{
		var $elements = $(elem).map(function()
		{
			var $this = $(this);
			var offset = $this.offset();
			var l = offset.left;
			var t = offset.top;
			var h = $this.height();
			var w = $this.width();

			var maxx = l + w;
			var maxy = t + h;

			return (mouseY <= maxy && mouseY >= t) && (mouseX <= maxx && mouseX >= l) ? $this : null;
		});

		return $elements;
	},
	sortJSON: function(data, key, way)
	{
		if(!isNaN(parseFloat(data[0][key])) && isFinite(data[0][key]))
		{
			return data.sort(function(a,b)
			{
				var x = a[key]; var y = b[key];
				if (way === 'asc' || typeof way === 'undefined') { return parseFloat(x) - parseFloat(y); }
				if(way === 'desc') { return parseFloat(y) - parseFloat(x); }
			});
		}
		else
		{
			return data.sort(function(a, b)
			{
				var x = a[key]; var y = b[key];
				if(way === 'desc' || typeof way === 'undefined') { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
				if(way === 'asc') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
			});
		}
	},
	draggableFocus: function(elem, stack)
	{
		var group = $.makeArray($(stack)).sort(function(a,b)
		{
			return (parseInt($(a).css("zIndex"),10) || 0) - (parseInt($(b).css("zIndex"),10) || 0);
		});
		if (!group.length) { return; }
		var min = parseInt($(group[0]).css("zIndex"), 10) || 0;
		$(group).each(function(i) { $(this).css("zIndex", min + i); });
		$(elem).css("zIndex", (min + group.length));
	},
	getKey: function(n)
	{
		if(n >= 65 && n <= 90)
		{
			return String.fromCharCode(n);
		}
		else
		{
			switch(n)
			{
				case $.ui.keyCode.BACKSPACE:
					return 'Backspace';
				break;

				case $.ui.keyCode.COMMA:
					return ',';
				break;

				case $.ui.keyCode.DELETE:
					return 'Del';
				break;

				case $.ui.keyCode.END:
					return 'End';
				break;

				case $.ui.keyCode.ENTER:
					return 'Enter';
				break;

				case $.ui.keyCode.HOME:
					return 'Home';
				break;

				case $.ui.keyCode.PAGE_DOWN:
					return 'PgDn';
				break;

				case $.ui.keyCode.PAGE_UP:
					return 'PgUp';
				break;

				case $.ui.keyCode.PERIOD:
					return '.';
				break;

				case $.ui.keyCode.SPACE:
					return 'Space';
				break;

				case $.ui.keyCode.TAB:
					return 'Tab';
				break;
			}
		}

		return false;
	},
	getVersion: function(showresult, callback)
	{
		$.ajax(
		{
			type: "GET",
			url: "changelog.xml",
			dataType: "xml",
			cache: false,
			success: function(xml)
			{
				var r = -1;

				if($(xml).find('version').length > 0)
				{
					if(showresult != true)
					{
						var n = $($(xml).find('version')[0]).attr('number');
						r = n;
					}
					else
					{
						r = xml;
					}
				}

				LMUIVersion = r;
				if(typeof callback === 'function') { callback.call(this, r); }
				else if(typeof showresult === 'function') { showresult.call(this, r); }
				else { return r; }
			},
			error: function(a,b,c)
			{
				var r = '(Unknown Version)';
				LMUIVersion = r;
				if(typeof callback === 'function') { callback.call(this, r); }
				else if(typeof showresult === 'function') { showresult.call(this, r); }
				else { return r; }
			}
		});
	},
	startMouseTracking: function(o)
	{
		mouseX = 0;
		mouseY = 0;

		$(o.attachto).mousemove(function(e)
		{
			mouseX = e.pageX;
			mouseY = e.pageY;
		});
	},
	popup: function(element, msg, autohide, posshift, forcedelete, callback)
	{
		var settings = JUtils.loadSettings('settings');
		var contentawareX = false;

		if(!(element instanceof $))
		{
			element = $(element);
		}

		var xPos = Math.round(element.offset().left);
		var yPos = Math.round(element.offset().top);
		var sPos = {	x : 0,
						y : 0,
						halign : 'left',
						valign : 'top'
				   };

		if($.isPlainObject(posshift))
		{
			if(($.isNumeric(posshift['x'])	|| typeof posshift['x'] === 'undefined') &&
			   ($.isNumeric(posshift['y'])	|| typeof posshift['y'] === 'undefined'))
			{
				sPos = $.extend(sPos, posshift);
			}

			if(typeof posshift['cax'] !== 'undefined' && posshift['cax'] === true)
			{
				contentawareX = true;
			}
		}

		var name = (typeof element.attr('id') !== 'undefined') ? element.attr('id') : element.attr('class');
		var layerexists = ($('#popup-'+name).length > 0);

		if(layerexists)
		{
			if(typeof forcedelete !== 'undefined' && forcedelete === true)
			{
				var popup = $('#popup-'+name).stop().remove();
				layerexists = false;
			}
			else
			{
				var popup = $('#popup-'+name).stop();
				var w = popup.width();
				var h = popup.height();
			}
		}

		if(!layerexists)
		{
			var popup = $('<div id="popup-'+name+'" class="popupmsg">'+msg+'</div>')
							.appendTo('body')
							.css({	'top' : parseInt(yPos) + parseInt(sPos['y']),
									'left' : parseInt(xPos) + parseInt(sPos['x']),
									'opacity' : 0
								})
							.show();

			var w = popup.width();
			var h = popup.height();

			if(contentawareX)
			{
				popup.css('left', parseInt(xPos) - (w/2));
			}

			popup.css('opacity', 1).hide();
		}

		if(sPos['halign'] == 'center' || sPos['halign'] == 'middle')
		{
			popup.css({	'width'			: w,
						'left'			: '50%',
						'margin-left'	: -(w/2)
					});
		}

		if(sPos['valign'] == 'center' || sPos['valign'] == 'middle')
		{
			popup.css({	'height'		: h,
						'top'			: '50%',
						'margin-top'	: -(h/2)
					});
		}

		popup.fadeIn(settings.fxtimer);

		if(typeof autohide !== 'undefined')
		{
			if(autohide == true)
			{
				popup
					.delay(300)
					.fadeOut(settings.fxtimer, function()
					{
						$(this).remove();
					});
			}
		}

		if(typeof callback === 'function')
		{
			callback.call(this);
		}
	},
	deletePopup: function(element)
	{
		if(!(element instanceof $))
		{
			element = $(element);
		}

		var name = (typeof element.attr('id') !== 'undefined') ? element.attr('id') : element.attr('class');
		$('#popup-'+name).stop().remove();
	},
	loadScripts: function(callback)
	{
		var len		= Defaults.windowList().length;
		var num		= 0;
		var error	= false;
		var noload	= [];

		$.each(Defaults.windowList(), function(i, v)
		{
			if(typeof v['name'] === 'undefined')
			{
				JUtils.popup($('body'),
							'LMUI Error:<br/>The WindowList in Defaults is missing a Filename',
							false,
							{	halign: 'center',
								valign: 'center'
							});
				return false;
			}
			else
			{
				$.ajax(
				{
					url: 'js/content/'+v['name']+'.js',
					dataType: 'script'
				})
				.success(function()
				{
					num++;

					if(!error)
					{
						if(len == num)
						{
							if(typeof callback === 'function')
							{
								callback.call(this);
							}
						}
					}
					else
					{
						if(len == num)
						{
							JUtils.showScriptError(callback, noload);
						}
					}
				})
				.error(function()
				{
					noload.push(v['name']);
					num++;

					if(!error)
					{
						error = true;
					}
					else
					{
						if(len == num)
						{
							JUtils.showScriptError(callback, noload);
						}
					}
				});
			}
		});
	},
	showScriptError: function(callback, noload)
	{
		if(noload.length == Defaults.windowList().length)
		{
			JUtils.popup($('body'),
						'Unable to Load LMUI Content Files.<br/>Make sure you\'re '+
						'running it through a webserver, e.g. <a href="https://www.apachefriends.org">xampp</a>.',
						false,
						{	halign: 'center',
							valign: 'center'
						});
		}
		else
		{
			var ul = '';

			$.each(noload, function(i, v)
			{
				ul += '<br/><span class="elist">' + v + '.js</span>';
			});

			JUtils.popup($('body'),
						'Unable to Load:' + ul+
						'<br/><br/>'+
						'Continue Anyway? '+
						'<br/>(errors may occur)'+
						'<br/><input type="button" id="trybutton" value="Continue"/>',
						false,
						{	halign: 'center',
							valign: 'center'
						},
						false,
						function()
						{
							$('#trybutton')
								.bind('click', function()
								{
									$('.errormsg').fadeOut(200, function()
									{
										$(this).remove();
									});
									callback.call(this);
								});
						});
		}
	}
}