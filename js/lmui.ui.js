UI = {
	loadUI: function(options)
	{
		/********* CHANGE WHEN USED IN GAME ********/
		var showworld = true;
		/*******************************************/
		/* FALSE = USE IN GAME, TRUE = DEVELOPMENT */
		/*******************************************/

		// Load Scripts and do Error Checks
		JUtils.loadScripts(function()
		{
			// Check for Updates
			UI.checkUpdates(function()
			{
				// Load Settings
				Defaults.WindowSettings().showworld = showworld;
				var settings = JUtils.loadSettings('settings');
				var o = $.extend(Defaults.WindowSettings().settings, options, settings);
				Defaults.World.current = o.scene;
				// Load Theme
				Themes.selectTheme(o.theme, false, function(themesettings)
				{
					// Update Settings with Theme
					var s = $.extend(o, themesettings, settings, {"showworld" : showworld});
					JUtils.saveSettings('settings', s);
					// Start Mouse Tracking (Quick Keys etc)
					JUtils.startMouseTracking(s);
					// Create Content
					UI.createHud(s.attachto);
					UI.createWindows(true);
					UI.rescaleUI(s.scale, Defaults.WindowSettings().settings.scale);
					UI.resizeMargins(s.margins, Defaults.WindowSettings().settings.margins);
					UI.updateScrollbars();
					Map.resizeMap();
					// Create Tooltips
					$(s.attachto).tooltip(JUtils.tooltipSettings);
					// Create Settings Menu
					Settings.create(s);
					// Page Wide Bindings
					UI.bindKeyPress(s);
					// Show World Background (or not)
					UI.showWorld(s);
				});
			});
		});
	},
	showWorld: function(o)
	{
		if(o.showworld == true)
		{
			$('html, body').css('background', '#222');

			if(o.world)
			{
				World.start(o);
			}
			else
			{
				UI.createBackground(o);
			}
		}
	},
	popuplateWorkspaces: function(defaultWorkspaces)
	{
		for(var i = defaultWorkspaces.length ; i < Defaults.Workspaces ; i++)
		{
			defaultWorkspaces.push(
			{
				"number"	: (i+1),
				"name"		: "No Name",
				"button"	: -1,
				"windows"	: []
			});
		}

		return defaultWorkspaces;
	},
	createHud: function(att)
	{
		var container = $('<div id="playerinfocontainer"/>')
							.css({	'left'		: UI.x(20)+'px',
									'top'		: 'auto',
									'bottom'	: UI.y(20)+'px',
									'width'		: '400px',
									'height'	: '150px'
							})
							.appendTo(att);

		$('<div id="compass"/>')
			.css({	'left' : '0',
					'top' : '0',
					'height' : '150px',
					'width' : '100%',
					'background-image' : 'url(images/playerinfo/compass.png)',
					'background-repeat' : 'no-repeat',
					'background-size' : 'auto 100%',
			})
			.appendTo(container);

		$('<div id="compassneedle"/>')
			.css({	'left' : '64px',
					'top' : '29px',
					'height' : '100px',
					'width' : '11px',
					'background-repeat' : 'no-repeat',
					'background-size' : 'auto 100%',
			})
			.appendTo(container);

		var health = $('<div id="healthouter"/>')
						.css({	'left' : '125px',
								'top' : '50px',
								'width' : '205px',
								'border-bottom-left-radius' : '10px'
						})
						.appendTo(container);

		var healthcalc = Math.round((parseInt(DB.player().health)/parseInt(DB.player().maxhealth))*100);

		$('<div id="health"/>')
			.css({	'height' : '100%',
					'width' : healthcalc + '%',
					'background-image' : 'url(images/playerinfo/health.png)',
					'border-bottom-left-radius' : '10px'
			})
			.appendTo(health);

		$('<div class="playerinfotext">'+DB.player().health+' / '+DB.player().maxhealth+'</div>')
			.css({	'left' : '125px',
					'top' : '52px',
					'width' : '200px'
			})
			.appendTo(container);

		var magicka = $('<div id="magickaouter"/>')
						.css({	'left' : '126px',
								'top' : '80px',
								'width' : '204px',
								'border-top-left-radius' : '10px'
						})
						.appendTo(container);

		var magickacalc = Math.round((parseInt(DB.player().magicka)/parseInt(DB.player().maxmagicka))*100);

		$('<div id="magicka"/>')
			.css({	'height' : '100%',
					'width' : magickacalc + '%',
					'background-image' : 'url(images/playerinfo/magicka.png)',
					'border-top-left-radius' : '10px'
			})
			.appendTo(magicka);

		$('<div class="playerinfotext">'+DB.player().magicka+' / '+DB.player().maxmagicka+'</div>')
			.css({	'left' : '125px',
					'top' : '82px',
					'width' : '200px'
			})
			.appendTo(container);

		var stamina = $('<div id="staminaouter"/>')
						.css({	'left' : '108px',
								'top' : '110px',
								'width' : '222px',
								'border-top-left-radius' : '30px'
						})
						.appendTo(container);

		var staminacalc = Math.round((parseInt(DB.player().stamina)/parseInt(DB.player().maxstamina))*100);

		$('<div id="stamina"/>')
			.css({	'height' : '100%',
					'width' : staminacalc + '%',
					'background-image' : 'url(images/playerinfo/stamina.png)',
					'border-top-left-radius' : '30px'
			})
			.appendTo(stamina);

		$('<div class="playerinfotext">'+DB.player().stamina+' / '+DB.player().maxstamina+'</div>')
			.css({	'left' : '125px',
					'top' : '112px',
					'width' : '200px'
			})
			.appendTo(container);

		$('#compassneedle').mouseover(function() {JUtils.rotateCompass();});

		$('<div class="currentitem"></div>').appendTo(container);
	},
	toggleLights: function(becomevisible, o, callback)
	{
		if(becomevisible === true)
		{
			$('.playerinfotext').stop().fadeIn(o.fxtimer);

			$('<div id="pausedLayer"/>')
				.appendTo(o.attachto)
				.fadeIn(o.fxtimer);

			if(o.fxshow == true)
			{
				FX.show(o.scene);
			}

			if(typeof(callback) === 'function')
			{
				callback.call(this);
			}
		}
		else if(becomevisible === false)
		{
			if(o.fxshow == true)
			{
				FX.hide(o.scene);
			}

			$('.playerinfotext').stop().fadeOut(o.fxtimer);

			$('#pausedLayer').fadeOut(o.fxtimer, function()
			{
				$(this).remove();

				if(typeof(callback) === 'function')
				{
					callback.call(this);
				}
			});
		}
	},
	createBackground: function(o)
	{
		if(o.showworld == true)
		{
			if($('#bgimage').length > 0) { $('#bgimage').remove(); }

			$('<img id="bgimage"/>')
				.attr(	'src', 'images/currentview/'+o.scene+'.jpg')
				.css({	'position' : 'absolute',
						'top' : '0',
						'left' : '0',
						'z-index' : '20',
						'width' : '100%',
						'height' : '100%'
				})
				.appendTo(o.attachto);
		}
	},
	removeBackground: function(o)
	{
		if(o.showworld == true)
		{
			$('#bgimage').remove();
		}
	},
	createWindows: function(showstickywindows)
	{
		var o = JUtils.loadSettings('settings');
		var w = JUtils.loadSettings('windows');

		// Create Windows in Workspaces
		var list = [];

		$.each(o['workspaces'], function(n1, workspace)
		{
			$.each(workspace['windows'], function(n2, windowname)
			{
				list.push(windowname);

				$(o.attachto).drawWindow(
					$.extend(
						JUtils.loadSettings(windowname),
						{	'workspace' : workspace['number'],
							'type' : windowname
						})
				);
			});
		});

		// Create Windows not in Workspaces
		$.each(Defaults.windowList(), function(i, v)
		{
			if(typeof v['name'] !== 'undefined' && v['create'] === true)
			{
				if($.inArray(v['name'], list) == -1)
				{
					$(o.attachto).drawWindow({'type' : v['name']});
				}
			}
		});

		// Update Scrollbars
		UI.updateScrollbars();

		// Show Sticky Windows
		if(showstickywindows === true && o['sticky'].length > 0)
		{
			$.each(o['sticky'], function(n, windowname)
			{
				$('#'+windowname).show();
			});
		}
	},
	checkUpdates: function(callback)
	{
		JUtils.getVersion(function(version)
		{
			var settings = JUtils.loadSettings('settings');

			if(typeof settings['version'] == 'undefined')
			{
				JUtils.saveSettings('settings', {'version': version});
			}
			else if(settings['version'] != version)
			{
				for(var cookie in $.cookie())
				{
				  $.removeCookie(cookie);
				}

				JUtils.saveSettings('settings', {'version': version});
			}

			if(typeof callback === 'function')
			{
				callback.call(this);
			}
		});
	},
	createScrollbars: function(container)
	{
		container = (typeof container !== 'undefined') ? container : '.content';

		$(container).mCustomScrollbar('destroy');

		$(container).each(function()
		{
			var h = $(this).closest('.uiWindow').height() - 70;
			var scrollbar = {
					set_height: h,
					scrollInertia: 0
			};

			if($(this).hasClass('inventoryitems'))
			{
				var invsettings = JUtils.loadSettings('inventory');
				if(invsettings['horizontal'] == true && invsettings['view'] == 'grid')
				{
					var settings	= JUtils.loadSettings('settings');
					var ratio		= (settings.scale / 100);
					var itemsize	= (50 * ratio);
					var padding		= 16;
					var itemsquare	= itemsize + padding;
					var invheight	= $('.inventoryitems').height() - 10;
					var times		= Math.floor(invheight / itemsquare)
					times			= (times == 0) ? 1 : times;
					var num			= $('.inventoryitem_outer:visible').length;
					var newwidth	= Math.ceil(num/times) * itemsquare;

					scrollbar['horizontalScroll'] = true;
					scrollbar['autoExpandHorizontalScroll'] = true;
					$(this).mCustomScrollbar(scrollbar);
					$('#inventory .inventoryitems .mCSB_container').width(newwidth);
					$(this).mCustomScrollbar('update');
				}
				else
				{
					$(this).mCustomScrollbar(scrollbar);
				}
			}
			else
			{
				$(this).mCustomScrollbar(scrollbar);
			}
		});
	},
	updateScrollbars: function(type)
	{
		if(typeof type !== 'undefined')
		{
			var l = $('#'+type);

			if(l.length > 0)
			{
				var ishidden = l.is(':hidden');

				if(ishidden)
				{
					var opacity = l.css('opacity');
					l.css('opacity', 0).show();
				}

				switch(type)
				{
					case 'inventory':
						var settings = JUtils.loadSettings('settings');
						UI.createScrollbars('#inventory .inventoryitems');
						var inner = $('.inventoryitems').offset().top;
						var outer = $('#inventory').offset().top;
						var maxw  = $('.inventoryitems').width();
						var marl  = parseInt($('#inventory .content').css('margin-left'));
						var marr  = parseInt($('#inventory .content').css('margin-right'));
						var wid   = $('#inventory').width();
						var totw  = (wid-(marl+marr));

						$('.invmenuicons, .invsorting')	.css({	'width': (totw-40)+'px'});
						$('.inventorysortinglist')		.css({	'width' : (totw-75) +'px',
																'margin-left' : 40 +'px'});
						$('.inventoryitems')			.css({	'height': '-='+((inner-outer)-40)+'px',
																'width': totw+'px'});
						$('.inventoryitem_outer_list')	.width(totw-48);
						$('#inventory .inventoryitems')	.mCustomScrollbar('update');
					break;

					case 'spells':
						UI.createScrollbars('#spells .content');
					break;

					case 'player':
						UI.createScrollbars('#player .content');
					break;

					case 'apparel':
						UI.createScrollbars('#apparel .content');
					break;

					case 'journal':
						UI.createScrollbars('#journalquests, #journaltext');
					break;

					case 'map':
						Map.resizeMap();
					break;

					case 'changelog':
						UI.createScrollbars('#changelog .content');
					break;

					default:
						UI.createScrollbars('#'+type+' .content');
				}

				if(ishidden)
				{
					l.css('opacity', opacity).hide();
				}
			}
		}
		else
		{
			for(var property in Defaults.WindowSettings())
			{
				UI.updateScrollbars(property);
			}
		}
	},
	toggleSticky: function(type)
	{
		var s = JUtils.loadSettings('settings');
		var obj = $('#' + type + ' .sticky');

		if(!obj.hasClass('sticky_on'))
		{
			obj.addClass('sticky_on');
			JUtils.changeToolTip(obj, 'Unstick Window');
			s['sticky'].push(type);
		}
		else
		{
			if(!UI.isPaused()) { $('#' + type).fadeOut(200); }
			obj.removeClass('sticky_on');
			JUtils.changeToolTip(obj, 'Stick Window');
			s['sticky'] = $.grep(s['sticky'], function(value) { return value != type; });
		}

		JUtils.saveSettings('settings', s);
	},
	bindKeyPress: function(o)
	{
		$(o.attachto).bind('keydown', function(e)
		{
			var s = JUtils.loadSettings('settings');

			 // 0-9 sets quick keys or selects a weapon
			if(e.which > 47 && e.which < 58)
			{
				e.preventDefault();
				var c = String.fromCharCode(e.keyCode);

				if(UI.isPaused()) // Inside Menu
				{
					var item = JUtils.getElement('.inventoryitem_outer, .spell');

					if(item.length)
					{
						if($(item[0]).hasClass('weapon') || $(item[0]).hasClass('spell'))
						{
							$.each($('.quickkey'), function()
							{
								if($(this).html() == c)
								{
									$(this).html('').hide();
									return false;
								}
							});

							$(item[0]).find('.quickkey').html(c).stop().fadeIn(200);
						}
					}
				}
				else
				{
					$.each($('.quickkey'), function() // Not Inside Menu (Is Playing)
					{
						if($(this).html() == c)
						{
							var invitem = $(this).closest('.inventoryitem_outer, .spell');

							if(invitem.hasClass('weapon'))
							{
								Defaults.World.weapon = invitem.attr('icon');
								var selquick = invitem.find('.inventoryitem_inner').clone().addClass('currentitem_sel');
								selquick.find('.quickkey, .favicon').remove();
								$('.currentitem').html(selquick);
								$('.selectedItem').removeClass('selectedItem');
								invitem.addClass('selectedItem');
							}
							else if(invitem.hasClass('spell'))
							{
								Defaults.World.weapon = 'spell';
								var selquick = invitem.find('img').clone().addClass('currentitem_sel');
								$('.currentitem').html(selquick);
								$('.selectedItem').removeClass('selectedItem');
								invitem.addClass('selectedItem');
							}

							return false;
						}
					});
				}
			}

			// Backspace Opens Settings Menu
			if(e.which == 8)
			{
				e.preventDefault();
				Settings.openSettings(s);
				$.sidr('toggle', 'sidr');
			}

			// Buttons to open the various workspaces
			$.each(s["workspaces"], function(n, buttons)
			{
				if(e.which == buttons["button"])
				{
					e.preventDefault();
					UI.toggleWorkSpace(parseInt(buttons["number"])-1);
					return false;
				}
			});
		});
	},
	toggleWorkSpace: function(workspace, show)
	{
		var o = JUtils.loadSettings('settings');

		if($('.uiAnimating').length == 0) // disallow button press while animating
		{
			JUtils.deletePopup('#mappopup');

			if(UI.isPaused() != (workspace+1) || show === true) // open new or change workspace
			{
				// Settings Menu Update
				$('.moveWorkspace_inner').removeClass('moveWorkspace_inner_hover');
				$('.moveWorkspace_inner:eq('+workspace+')').addClass('moveWorkspace_inner_hover');
				Settings.updateWorkspaceSelect(workspace+1);
				Settings.updateWorkspaceInputs(workspace+1);
				if($('#changelog').length > 0) { $('#changelog').remove(); }

				if(UI.isPaused() == false) // On Open
				{
					var fxtim = ($('.uiWindow').length > 0) ? o.fxtimer : 0;

					$.each($('.uiWindow'), function(i, w)
					{
						if(!$(w).hasClass('workspace'+(workspace+1)))
						{
							$(w).fadeOut(o.fxtimer);
						}
						else
						{
							$(w).fadeIn(o.fxtimer);
						}
					});

					UI.isPaused(false, workspace+1);
					UI.toggleLights(true, o);
					$('.uiAnimating').remove();
				}
				else // On Change
				{
					$('.uiWindow')
						.fadeOut(o.fxtimer)
						.promise()
						.done(function()
						{
							$('.workspace'+(workspace+1))
								.fadeIn(o.fxtimer)
								.promise()
								.done(function()
								{
									UI.isPaused(true);
									UI.isPaused(false, workspace+1);
									$('.uiAnimating').remove();
								});
						});
				}
			}
			else // pressed same button as workspace we're on (close it)
			{
				$('.moveWorkspace_inner').removeClass('moveWorkspace_inner_hover');
				UI.toggleLights(false, o);

				$.each($('.uiWindow'), function(i, w)
				{
					if($(w).find('.sticky_on').length == 0)
					{
						$(w).fadeOut(o.fxtimer);
					}
					else
					{
						$(w).fadeIn(o.fxtimer);
					}
				});

				UI.isPaused(true);
				$('.uiAnimating').remove();
			}
		}
	},
	rescaleUI: function(scale, old)
	{
		scale = parseInt(scale);
		old = parseInt(old);

		var iiw		= parseInt($('.inventoryicons').css('width'));
		var iih		= parseInt($('.inventoryicons').css('height'));
		$('.inventoryicons').css({	'width'			: ((iiw / old) * scale) + 'px',
									'height'		: ((iih / old) * scale) + 'px'});

		var ilw		= parseInt($('.listicon').css('width'));
		var ilh		= parseInt($('.listicon').css('height'));
		$('.listicon').css({		'width'			: ((ilw / old) * scale) + 'px',
									'height'		: ((ilh / old) * scale) + 'px'});

		var igw		= parseInt($('.inventoryitem_outer_grid').css('width'));
		var igh		= parseInt($('.inventoryitem_outer_grid').css('height'));
		$('.inventoryitem_outer_grid').css({'width'			: ((igw / old) * scale) + 'px',
											'height'		: ((igh / old) * scale) + 'px'});
	},
	resizeMargins: function(scale, old)
	{
		scale = parseInt(scale);
		old = parseInt(old);

		/** General **/
		var ml		= parseInt($('.content').css('margin-left'));
		var mr		= parseInt($('.content').css('margin-right'));
		$('.content').css({	'margin-left'	: ((ml * scale) / old) + 'px',
							'margin-right'	: ((mr * scale) / old) + 'px'
		});

		/** Inventory **/

		// Categories

		var iiml	= parseInt($('.invmenuicons').css('margin-left'));
		$('.invmenuicons').css({'margin-left'	: ((iiml * scale) / old) + 'px'});

		// Sortings

		if($('.invsorttablegrid').length > 0)
		{
			var invsorttype = '.invsorttablegrid';
		}
		else
		{
			var invsorttype = '.invsorttablelist';
		}

		var ispl	= parseInt($(invsorttype).css('padding-left'));
		$(invsorttype).css({'padding-left'	: ((ispl * scale) / old) + 'px'});
	},
	isPaused: function(currently_on, ws)
	{
		if(typeof currently_on === 'undefined')
		{
			if($('#ininventory').length > 0)
			{
				return $('#ininventory').attr('workspace');
			}
			else
			{
				return false;
			}
		}
		else if(currently_on == true)
		{
			Defaults.World.paused = false;
			$('#ininventory').remove();
		}
		else
		{
			Defaults.World.paused = true;
			ws = (typeof ws !== 'undefined') ? ws : 1;
			var o = JUtils.loadSettings('settings');
			$('<div id="ininventory" workspace="'+ws+'"/>')
				.appendTo(o.attachto);
		}
	},
	bindClick: function()
	{
		$('.inventoryitem_outer')
			.unbind('click')
			.bind('click', function(e)
			{
				e.preventDefault();

				switch(e.which)
				{
					case 1:
						if($(this).hasClass('weapon'))
						{
							Defaults.World.weapon = $(this).attr('icon');
							var sel = $(this).find('.inventoryitem_inner').clone().addClass('currentitem_sel');
							sel.find('.quickkey, .favicon').remove();
							$('.currentitem').html(sel);
							$('.selectedItem').removeClass('selectedItem');
							$(this).addClass('selectedItem');
						}
					break;

					case 2:
						if($(this).hasClass('favorite'))
						{
							$(this).removeClass('favorite');
							$(this).find('.favicon').stop().fadeOut(200);
							if($('.favitem').is('.inviconsel')) { $(this).hide(200); }
						}
						else
						{
							$(this).addClass('favorite');
							$(this).find('.favicon').stop().fadeIn(200);
						}
					break;
				}
			})
			.bind('contextmenu', function(e)
			{
				e.preventDefault();

				$(this).hide(200, function()
				{
					UI.updateScrollbars('inventory');
					$(this).remove();
				});
			});

		$('.spell')
			.unbind('click')
			.bind('click', function(e)
			{
				e.preventDefault();

				if(e.which == 1)
				{
					Defaults.World.weapon = 'spell';
					var selquick = $(this).find('img').clone().addClass('currentitem_sel');
					$('.currentitem').html(selquick);
					$('.selectedItem').removeClass('selectedItem');
					$(this).addClass('selectedItem');
				}
			});
	},
	addWindowExtras: function(o)
	{
		var header = '';
		var footer = '';

		switch(o.type)
		{
			case 'inventory':
				header += '<div class="changescrollbar"/>';
				header += '<div class="filterinventory"/>';
			break;
			case 'map':
				header += '<div class="searchmap"/>';
				footer += '<div class="mapfilters"/>';
			break;
		}

		return [header, footer];
	},
	header: function(type)
	{
		return $('#' + type + ' .header > span');
	},
	content: function(type)
	{
		return $('#' + type + ' .content');
	},
	normWHXY: function(obj)
	{
		obj['x']		= UI.x(obj['x']);
		obj['y']		= UI.y(obj['y']);
		obj['width']	= UI.width(obj['width']);
		obj['height']	= UI.height(obj['height']);
		return obj;
	},
	width: function(pos)
	{
		var w = $(document).width();
		return Math.round((pos/Defaults.xConst)*w);
	},
	height: function(pos)
	{
		var h = $(document).height();
		return Math.round((pos/Defaults.yConst)*h);
	},
	x: function(pos)
	{
		var w = $(document).width();
		return Math.round((pos/Defaults.xConst)*w);
	},
	y: function(pos)
	{
		var h = $(document).height();
		return Math.round((pos/Defaults.yConst)*h);
	}
};