(function($)
{
	$.fn.extend(
	{
		drawWindow: function(opt, nocontent)
		{
			// Load Window Settings
			var defaults = $.extend(Defaults.unknownWindow(), opt);
			opt.type = (opt.type != '' && typeof opt.type == 'string') ? opt.type : defaults.type;
			var windowsettings = JUtils.loadSettings(opt.type);
			var allwindowsettings = JUtils.loadSettings('windows');
			var uisettings = JUtils.loadSettings('settings');

			return this.each(function()
			{
				var o = $.extend(defaults, opt, allwindowsettings, windowsettings);

				// Create Windows
				if($('#'+o.type).length == 0)
				{
					var extras = UI.addWindowExtras(o);

					$(this).append(	'<div id="'+o.type+'" windowtype="'+o.type+'" '+
									'class="uiWindow workspace'+o.workspace+'">'+
									extras[0]+
									'<div class="sticky"/>'+
									'<div class="imgtopleft" />'+
									'<div class="imgtopright" />'+
									'<div class="imgbottomleft" />'+
									'<div class="imgbottomright" />'+
									'<div class="header">'+
									'<span class="headertext"/>'+
									'</div>'+
									'<div class="content"/>'+
									extras[1]+
									'</div>'
					);
				}

				// Reference to Window
				var obj = $('#'+o.type);

				// Sticky Button
				var st = $('#' + o.type + ' .sticky');
				if(!$.inArray(o.type, uisettings['sticky'])) { st.addClass('sticky_on'); }
				var tmsg = (st.hasClass('sticky_on')) ? 'Unstick Window' : 'Stick Window';
				st.attr('title', tmsg).bind('click', function() {UI.toggleSticky(o.type)});

				// Get Window Content
				if(typeof nocontent === 'undefined' || nocontent != true)
				{
					var exists	= 0;

					$.each(Defaults.windowList(), function(i, v)
					{
						if(typeof v['name'] !== 'undefined' && v['name'] == o.type)
						{
							var fname = JUtils.capFirst(o.type);
							exists = -1;

							if(typeof window[fname] !== 'undefined')
							{
								if(typeof v['header'] !== 'undefined')
								{
									UI.header(o.type).html(v['header']);
								}

								if(typeof window[fname].create === 'function')
								{
									window[fname].create(UI.content(o.type), o.type);
									exists = 1;
								}
							}

							return false;
						}
					});

					switch(exists)
					{
						case 0:
							UI.header(o.type).html('Error #1');
							UI.content(o.type).html('Unable to find reference: ' + o.type);
						break;

						case -1:
							UI.header(o.type).html('Error #2');
							UI.content(o.type).html('Unable to populate "' + o.type + '"');
						break;
					}
				}

				// Window Position and Alignments
				obj.css('position', 'absolute');
				o.x += (JUtils.endsWith(o.x, 'px')) ? '' : 'px';
				o.y += (JUtils.endsWith(o.y, 'px')) ? '' : 'px';
				obj.css(o.alignX, o.x);
				obj.css(o.alignY, o.y);

				// Window Size
				o.width += (JUtils.endsWith(o.width, 'px')) ? '' : 'px';
				o.height += (JUtils.endsWith(o.height, 'px')) ? '' : 'px';
				obj.css('width', o.width);
				obj.css('height', o.height);

				// Theme CSS
				$.each(uisettings['css'], function(key, val)
				{
					obj.css(key, val);
				});

				// Update Window Scrollbars
				UI.updateScrollbars(o.type);

				// Draggable
				obj.draggable(
				{
					handle: '.header',
					stack: '.uiWindow',
					containment: 'document',
					snap: true,
					snapMode: 'outer',
					snapTolerance: 5,
					start: function(e, ui)
					{
						if($('.sidr').is(':visible'))
						{
							$('.moveWorkspace_inner')
								.mouseover(function()
								{
									$(this).addClass('moveWorkspacehover');
								})
								.mouseout(function()
								{
									$(this).removeClass('moveWorkspacehover');
								});
						}
					},
					stop: function(e, ui)
					{
						var haschanged = false;

						if($('.sidr').is(':visible'))
						{
							$('.moveWorkspace_inner')
								.removeClass('moveWorkspacehover', 200)
								.unbind('mouseover')
								.unbind('mouseout');

							var checkworkspace = JUtils.getElement('.moveWorkspace');

							if(checkworkspace.length)
							{
								var workspace = parseInt($(checkworkspace).text());
								var type = $(this).attr('windowtype');
								var settings = JUtils.loadSettings('settings');
								var oldworkspace = 0;

								// Check if workspace has changed, and if true, update settings
								$.each(settings['workspaces'], function(n1, button)
								{
									$.each(button['windows'], function(n2, wind)
									{
										if(wind == type)
										{
											if((n1+1) != workspace) // moved to different workspace
											{
												settings['workspaces'][n1]['windows'].splice(n2, 1);
												haschanged = true;
												oldworkspace = (n1+1);
											}
										}
									});
								});

								// Add the new window (if workspace changed)
								if(haschanged)
								{
									var origleft = ui.originalPosition.left;
									var origtop = ui.originalPosition.top;

									JUtils.saveSettings(type,
									{
										'x' : origleft,
										'y' : origtop
									});

									settings['workspaces'][workspace-1]['windows'].push(type);
									JUtils.saveSettings('settings', settings);
									$('#'+type)
										.removeClass('workspace'+oldworkspace)
										.addClass('workspace'+workspace)
										.fadeOut(200, function()
										{
											$(this).css({	top : ui.originalPosition.top,
															left: ui.originalPosition.left
											});
										});
								}
							}
						}

						if(!haschanged)
						{
							var wpos = {	'x':	ui.position.left,
											'y':	ui.position.top,
										};

							JUtils.saveSettings(o.type, wpos);
						}
					}
				})
				.bind('mousedown', function()
				{
					JUtils.draggableFocus($(this), '.uiWindow');
				});

				// Resizable
				obj.resizable(
				{
					helper: "resizerStyle",
					ghost: true,
					minHeight: 150,
					minWidth: 200,
					handles: 'n, ne, e, se, s, sw, w, nw',
					containment: 'document',
					stop: function(e, ui)
					{
						var wpos = {	width:	ui.size.width,
										height:	ui.size.height,
										x:		ui.position.left,
										y:		ui.position.top,
									};

						JUtils.saveSettings(o.type, wpos);
						UI.updateScrollbars(o.type);
					}
				});
			});
		}
	});
})(jQuery);