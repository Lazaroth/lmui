World = {
	start: function()
	{
		if($('#worldcanvas').length > 0) { World.stop(); }
		$('<canvas id="worldcanvas" width="1" height="1"/>').appendTo('body');
		Defaults.World.active = true;
		var worldcanvas = document.getElementById('worldcanvas');
		var worldplayer = new World.WorldPlayer(15.3, -1.2, Math.PI * 0);
		var map = new World.Map(Defaults.World.size);
		var controls = new World.Controls();
		var camera = new World.Camera(worldcanvas, World.MOBILE ? 160 : 320, Math.PI * 0.4);
		var loop = new World.GameLoop();

		map.randomize();

		loop.start(function frame(seconds)
		{
			if(Defaults.World.active && typeof map !== 'undefined')
			{
				map.update(seconds);
				worldplayer.update(controls.states, map, seconds);
				camera.render(worldplayer, map);
			}
			else
			{
				cancelAnimationFrame(loop);
				worldplayer	= undefined;
				camera		= undefined;
				controls	= undefined;
				map			= undefined;
				loop		= undefined;
				$(document).unbind("keypress.key37");
				$(document).unbind("keypress.key38");
				$(document).unbind("keypress.key39");
				$(document).unbind("keypress.key40");
			}
		});

		$('#worldcanvas')
			.stop()
			.css({
				'opacity' : '1',
				'display' : 'none'
			})
			.fadeIn(200);
	},
	stop: function()
	{
		Defaults.World.active = false;
		$('#worldcanvas').remove();
	},
	Controls: function()
	{
		this.codes  = { 37: 'left', 39: 'right', 38: 'forward', 40: 'backward' };
		this.states = { 'left': false, 'right': false, 'forward': false, 'backward': false };
		document.addEventListener('keydown', this.onKey.bind(this, true), false);
		document.addEventListener('keyup', this.onKey.bind(this, false), false);
		document.addEventListener('touchstart', this.onTouch.bind(this), false);
		document.addEventListener('touchmove', this.onTouch.bind(this), false);
		document.addEventListener('touchend', this.onTouchEnd.bind(this), false);
	},
	WorldPlayer: function(x, y, direction)
	{
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.weapon = new World.Bitmap('images/world/'+Defaults.World.weapon+'.png', 200, 200);
		this.paces = 0;
	},
	Bitmap: function(src, width, height)
	{
		this.image = new Image();
		this.image.src = src;
		this.width = width;
		this.height = height;
	},
	Map: function(size)
	{
		this.size = size;
		this.wallGrid = new Uint8Array(size * size);
		this.skybox = new World.Bitmap('images/world/'+Defaults.World.current+'.jpg', 4000, 1290);
		this.wallTexture = new World.Bitmap('images/world/wall.jpg', 768, 256);
		this.light = 0;
	},
	Camera: function(canvas, resolution, fov)
	{
		this.ctx = canvas.getContext('2d');
		this.width = canvas.width = window.innerWidth * 0.5;
		this.height = canvas.height = window.innerHeight * 0.5;
		this.resolution = resolution;
		this.spacing = this.width / resolution;
		this.fov = fov;
		this.range = World.MOBILE ? 8 : 14;
		this.lightRange = Defaults.World.settings[Defaults.World.current].lightRange;
		this.scale = (this.width + this.height) / 600;
	},
	GameLoop: function()
	{
		this.frame = this.frame.bind(this);
		this.lastTime = 0;
		this.callback = function() {};
	},
	CIRCLE: Math.PI * 2,
	MOBILE: /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)
}

World.Controls.prototype.onTouch = function(e)
{
	var t = e.touches[0];
	this.onTouchEnd(e);
	if (t.pageY < window.innerHeight * 0.5) this.onKey(true, { keyCode: 38 });
	else if (t.pageX < window.innerWidth * 0.5) this.onKey(true, { keyCode: 37 });
	else if (t.pageY > window.innerWidth * 0.5) this.onKey(true, { keyCode: 39 });
};

World.Controls.prototype.onTouchEnd = function(e)
{
	this.states = { 'left': false, 'right': false, 'forward': false, 'backward': false };
	e.preventDefault();
	e.stopPropagation();
};

World.Controls.prototype.onKey = function(val, e)
{
	var state = this.codes[e.keyCode];
	if (typeof state === 'undefined') return;
	this.states[state] = val;
	e.preventDefault && e.preventDefault();
	e.stopPropagation && e.stopPropagation();
};

World.WorldPlayer.prototype.rotate = function(angle)
{
	this.direction = (this.direction + angle + World.CIRCLE) % (World.CIRCLE);
	$('#compassneedle').rotate(Math.round((this.direction / (2*Math.PI)) * 360));
};

World.WorldPlayer.prototype.walk = function(distance, map)
{
	var dx = Math.cos(this.direction) * distance;
	var dy = Math.sin(this.direction) * distance;
	if (map.get(this.x + dx, this.y) <= 0) this.x += dx;
	if (map.get(this.x, this.y + dy) <= 0) this.y += dy;
	this.paces += distance;
};

World.WorldPlayer.prototype.update = function(controls, map, seconds)
{
	this.weapon = new World.Bitmap('images/world/'+Defaults.World.weapon+'.png', 200, 200);

	if(!Defaults.World.paused)
	{
		if (controls.left) this.rotate(-Math.PI * seconds);
		if (controls.right) this.rotate(Math.PI * seconds);
		if (controls.forward) this.walk(3 * seconds, map);
		if (controls.backward) this.walk(-3 * seconds, map);
	}
};

World.Map.prototype.get = function(x, y)
{
	x = Math.floor(x);
	y = Math.floor(y);
	if (x < 0 || x > this.size - 1 || y < 0 || y > this.size - 1) return -1;
	return this.wallGrid[y * this.size + x];
};

World.Map.prototype.randomize = function()
{
	for (var i = 0; i < this.size * this.size; i++)
	{
		this.wallGrid[i] = Math.random() < 0.3 ? 1 : 0;
	}
};

World.Map.prototype.cast = function(point, angle, range)
{
	var self = this;
	var sin = Math.sin(angle);
	var cos = Math.cos(angle);
	var noWall = { length2: Infinity };

	return ray({ x: point.x, y: point.y, height: 0, distance: 0 });

	function ray(origin)
	{
		var stepX = step(sin, cos, origin.x, origin.y);
		var stepY = step(cos, sin, origin.y, origin.x, true);
		var nextStep = stepX.length2 < stepY.length2
		  ? inspect(stepX, 1, 0, origin.distance, stepX.y)
		  : inspect(stepY, 0, 1, origin.distance, stepY.x);

		if (nextStep.distance > range) return [origin];
		return [origin].concat(ray(nextStep));
	}

	function step(rise, run, x, y, inverted)
	{
		if (run === 0) return noWall;
		var dx = run > 0 ? Math.floor(x + 1) - x : Math.ceil(x - 1) - x;
		var dy = dx * (rise / run);
		return {
		  x: inverted ? y + dy : x + dx,
		  y: inverted ? x + dx : y + dy,
		  length2: dx * dx + dy * dy
		};
	}

	function inspect(step, shiftX, shiftY, distance, offset)
	{
		var dx = cos < 0 ? shiftX : 0;
		var dy = sin < 0 ? shiftY : 0;
		step.height = self.get(step.x - dx, step.y - dy);
		step.distance = distance + Math.sqrt(step.length2);
		if (shiftX) step.shading = cos < 0 ? 2 : 0;
		else step.shading = sin < 0 ? 2 : 1;
		step.offset = offset - Math.floor(offset);
		return step;
	}
};

World.Map.prototype.update = function(seconds)
{
	if(Defaults.World.settings[Defaults.World.current].raining)
	{
		if (this.light > 0) this.light = Math.max(this.light - 10 * seconds, 0);
		else if (Math.random() * 5 < seconds) this.light = 2;
	}
};

World.Camera.prototype.render = function(player, map)
{
	this.drawSky(player.direction, map.skybox, map.light);
	this.drawColumns(player, map);
	this.drawWeapon(player.weapon, player.paces);
};

World.Camera.prototype.drawSky = function(direction, sky, ambient)
{
	var width = this.width * (World.CIRCLE / this.fov);
	var left = -width * direction / World.CIRCLE;

	this.ctx.save();
	this.ctx.drawImage(sky.image, left, 0, width, this.height);
	if (left < width - this.width)
	{
		this.ctx.drawImage(sky.image, left + width, 0, width, this.height);
	}

	if (ambient > 0)
	{
		this.ctx.fillStyle = '#ffffff';
		this.ctx.globalAlpha = ambient * 0.1;
		this.ctx.fillRect(0, this.height * 0.5, this.width, this.height * 0.5);
	}

	this.ctx.restore();
};

World.Camera.prototype.drawColumns = function(player, map)
{
	this.ctx.save();

	for (var column = 0; column < this.resolution; column++)
	{
		var angle = this.fov * (column / this.resolution - 0.5);
		var ray = map.cast(player, player.direction + angle, this.range);
		this.drawColumn(column, ray, angle, map);
	}

	this.ctx.restore();
};

World.Camera.prototype.drawWeapon = function(weapon, paces)
{
	var bobX = Math.cos(paces * 2) * this.scale * 4;
	var bobY = Math.sin(paces * 4) * this.scale * 6;
	var left = this.width * 0.52 + bobX;
	var top = this.height * 0.03 + bobY;
	this.ctx.drawImage(weapon.image, left, top, weapon.width * this.scale, weapon.height * this.scale);
};

World.Camera.prototype.drawColumn = function(column, ray, angle, map)
{
	var ctx = this.ctx;
	var texture = map.wallTexture;
	var left = Math.floor(column * this.spacing);
	var width = Math.ceil(this.spacing);
	var hit = -1;

	while (++hit < ray.length && ray[hit].height <= 0);

	for(var s = ray.length - 1; s >= 0; s--)
	{
		var step = ray[s];

		if(s === hit)
		{
			var textureX = Math.floor(texture.width * step.offset);
			var wall = this.project(step.height, angle, step.distance);

			ctx.globalAlpha = 1;
			ctx.drawImage(texture.image, textureX, 0, 1, texture.height, left, wall.top, width, wall.height);

			ctx.fillStyle = Defaults.World.settings[Defaults.World.current].fadeTo;
			ctx.globalAlpha = Math.max((step.distance + step.shading) / this.lightRange - map.light, 0);
			ctx.fillRect(left, wall.top, width, wall.height);
		}

		ctx.fillStyle = '#ffffff';
		ctx.globalAlpha = 0.05;

		if(Defaults.World.settings[Defaults.World.current].raining)
		{
			var rainDrops = Math.pow(Math.random(), 3) * s;
			var rain = (rainDrops > 0) && this.project(0.1, angle, step.distance);
			while (--rainDrops > 0) ctx.fillRect(left, Math.random() * rain.top, 1, rain.height);
		}
	}
};

World.Camera.prototype.project = function(height, angle, distance)
{
	var z = distance * Math.cos(angle);
	var wallHeight = this.height * height / z;
	var bottom = this.height / 2 * (1 + 1 / z);
	return {
		top: bottom - wallHeight,
		height: wallHeight
	};
};

World.GameLoop.prototype.start = function(callback)
{
	this.callback = callback;
	requestAnimationFrame(this.frame);
};

World.GameLoop.prototype.frame = function(time)
{
	var seconds = (time - this.lastTime) / 1000;
	this.lastTime = time;
	if (seconds < 0.2) this.callback(seconds);
	requestAnimationFrame(this.frame);
};