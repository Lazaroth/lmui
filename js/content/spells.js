Spells = {
	create: function(content)
	{
		var numOfSpells	= JUtils.ranNum(15, 20);
		var allspells = [];
		var spellchk = new Array();

		for(var i = 0 ; i < numOfSpells ; i++)
		{
			var spellref	= JUtils.ranKey(DB.spell_list());
			var spellname	= DB.spell_list()[spellref][1];

			if($.inArray(spellname, spellchk) == -1)
			{
				var school		= DB.spell_list()[spellref][0];
				var spellicon	= spellname.toLowerCase().replace(/[^a-zA-Z]+/g, '');
				allspells.push({'name' : spellname, 'school' : school, 'icon' : spellicon});
				spellchk.push(spellname);
			}
		}

		allspells = JUtils.sortJSON(allspells, 'name', 'desc');

		for(var i = 0 ; i < allspells.length ; i++)
		{
			content
				.append(	'<div class="spell" title="'+allspells[i]['school']+'">'+
							'<div class="quickkey quickspell"></div>'+
							'<img src="images/icons_spells/'+allspells[i]['icon']+'.png" />'+
							allspells[i]['name']+
							'</div>'
			);
		}

		UI.bindClick();
	}
}