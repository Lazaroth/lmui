Player = {
	create: function(content, type)
	{
		var inc =	'<table cellspacing="0" cellpadding="0" border="0" class="playertable">'+
					'<tr><td class="player_skills"></td>'+
					'<td class="player_skillnumber"></td></tr>'+
					'<tr><td colspan="2">'+
					'<table cellspacing="0" cellpadding="0" border="0" class="playertable"><tr class="playerbold">'+
					'<td class="player_infolabel">Race</td>'+
					'<td class="player_infotext">'+DB.player().race+'</td>'+
					'</tr><tr class="playerbold">'+
					'<td class="player_infolabel">Class</td>'+
					'<td class="player_infotext">'+DB.player().classtype+'</td>'+
					'</tr><tr class="playerbold">'+
					'<td class="player_infolabel">Level</td>'+
					'<td class="player_infotext">'+DB.player().level+'</td>'+
					'</tr><tr class="playerbold">'+
					'<td class="player_infolabel">Gender</td>'+
					'<td class="player_infotext">'+DB.player().gender+'</td>'+
					'</tr></table></td></tr>'+
					'<tr><td class="skilltitle" colspan="2">Attributes</td></tr>';

		$.each(DB.player().attributes, function(key, val)
		{
			inc +=	'<tr>'+
					'<td class="player_skills">'+DB.attributes()[key]+'</td>'+
					'<td class="player_skillnumber">'+val+'</td>'+
					'</tr>';
		});

		var skills		= $.extend(DB.skills(), DB.player().skills);
		var skillorder	= ['primary', 'major', 'minor', 'misc'];

		$.each(skillorder, function(n, order)
		{
			inc +=	'<tr><td colspan="2">'+
					'<table cellspacing="0" cellpadding="0" border="0" class="playertable">'+
					'<tr><td class="player_skills"></td>'+
					'<td class="player_skillnumber"></td></tr>'+
					'<tr><td class="skilltitle" colspan="2">'+order+' Skills</td></tr>';

			$.each(skills, function(name, stats)
			{
				if(order == stats[0])
				{
					inc +=	'<tr><td class="player_skills">'+name+'</td>'+
							'<td class="player_skillnumber">'+stats[1]+'</td></tr>';
				}
			});

			inc +=	'</table>'+
					'</td></tr>';
		});

		content.append(inc+'</table>');
		Player.createHeader(type);
	},
	createHeader: function(type)
	{
		$('#' + type + ' .header > span').html(DB.player().name);
	}
}