Inventory = {
	create: function(att, type)
	{
		$(	'<table cellspacing="0" cellpadding="0" border="0" class="inventorytop">'+
			'<tr><td>'+
			'<div class="invmenuicons"/>'+
			'</td></tr>'+
			'<tr><td>'+
			'<div class="invsorting"/>'+
			'</td></tr>'+
			'<tr><td>'+
			'<div class="inventoryitems"/>'+
			'</td></tr>'+
			'</table>')
		.appendTo(att);

		var settings = JUtils.loadSettings('settings');

		// Create Inventory Type Icons
		Inventory.drawCategories(att, settings);

		// Set Initial cookie.view or get settings from cookie
		var invsettings = $.extend({	view : 'grid', sorting : 'desc', alt : 0},
										JUtils.loadSettings('inventory'));
		JUtils.saveSettings('inventory', invsettings);
		var combinedsettings = $.extend(settings, invsettings, {'type': type});

		// Create Inventory Sort Text
		Inventory.drawSorting(combinedsettings);

		// Draw Inventory Content
		var content = Inventory.populateInventory();
		var sorttype = (typeof invsettings['sorttype'] === 'undefined') ? Inventory.sortList()[0]['safe0'] : invsettings['sorttype'];
		JUtils.sortJSON(content, sorttype, invsettings['sortway']);
		Inventory.drawItems(content, invsettings);
		Inventory.updateSortingSize();
		Inventory.bindScrollbarDirection(combinedsettings);
		Inventory.filterContent(combinedsettings);
	},
	drawCategories: function(att, settings)
	{
		$('.inventoryicons').not('.inviconsel').css({'opacity' : '0.7'});
		var view = JUtils.loadSettings('inventory');
		if(view.view == 'list') { var ltype = 'invtype1'; }
		else { var ltype = 'invtype2'; }

		$(	'<table cellspacing="0" cellpadding="0" border="0" class="invmenuicontable">'+
			'<tr class="invmenuicontr">'+
			'<td/><td/><td/><td/>'+
			'<td/><td/><td/><td align="right"/>'+
			'</tr>'+
			'</table>')
		.appendTo('.invmenuicons');

		$('<div class="inventoryicons ishowall" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').show();
				UI.updateScrollbars('inventory');
			})
			.css('opacity', 1)
			.attr('title', 'Show All')
			.addClass('inviconsel')
			.appendTo('.invmenuicontr td:eq(0)');

		$('<div class="inventoryicons iweapons" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').hide();
				$('.weapon').show();
				UI.updateScrollbars('inventory');
			})
			.attr('title', 'Weapons')
			.appendTo('.invmenuicontr td:eq(1)');

		$('<div class="inventoryicons iarmors" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').hide();
				$('.armor').show();
				UI.updateScrollbars('inventory');
			})
			.attr('title', 'Armors')
			.appendTo('.invmenuicontr td:eq(2)');

		$('<div class="inventoryicons iscrollsandbooks" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').hide();
				$('.booksandscroll').show();
				UI.updateScrollbars('inventory');
			})
			.attr('title', 'Scrolls and Books')
			.appendTo('.invmenuicontr td:eq(3)');

		$('<div class="inventoryicons iingredients" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').hide();
				$('.ingredient').show();
				UI.updateScrollbars('inventory');
			})
			.attr('title', 'Ingredients')
			.appendTo('.invmenuicontr td:eq(4)');

		$('<div class="inventoryicons ipotions" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').hide();
				$('.potion').show();
				UI.updateScrollbars('inventory');
			})
			.attr('title', 'Potions')
			.appendTo('.invmenuicontr td:eq(5)');

		$('<div class="inventoryicons ifavorite favitem" />')
			.bind('click', function()
			{
				$('.inventoryitem_outer').hide();
				$('.favorite').show();
				UI.updateScrollbars('inventory');
			})
			.attr('title', 'Favorites')
			.appendTo('.invmenuicontr td:eq(6)');

		$('.inventoryicons')
			.bind('mouseover', function() { $(this).stop().animate({'opacity' : '1'}, settings.fxtimer); })
			.bind('mouseout', function() { if(!$(this).hasClass('inviconsel')) $(this).stop().animate({'opacity' : '0.7'}, settings.fxtimer); })
			.bind('click', function() { $('.inventoryicons').not(this).removeClass('inviconsel').animate({'opacity' : '0.7'}, settings.fxtimer); $(this).addClass('inviconsel'); })

		$('<div class="listicon '+ltype+'" />')
			.bind('click', function()
			{
				if($(this).hasClass('invtype1'))
				{
					$(this)
						.removeClass('invtype1')
						.addClass('invtype2');
					JUtils.changeToolTip($(this), 'Show as List');
					$('.changescrollbar').stop().fadeIn(settings.fxtimer);
				}
				else
				{
					$(this)
						.attr('title', 'Show as Grid')
						.removeClass('invtype2')
						.addClass('invtype1');
					JUtils.changeToolTip($(this), 'Show as Grid');
					$('.changescrollbar').stop().fadeOut(settings.fxtimer);
				}

				Inventory.updateInventory();
				Inventory.updateSortingSize();
			})
			.attr('title', $(this).hasClass('invtype1') ? 'Show as List' : 'Show as Grid')
			.tooltip(JUtils.tooltipSettings)
			.appendTo('.invmenuicontr td:eq(7)');
	},
	drawItems: function(content, settings)
	{
		content = (settings.sorting == 'asc') ? content.reverse() : content;
		var len = content.length, n, stats;

		for(n = 0, stats = content[0] ; n < len ; ++n, stats = content[n])
		{
			if('icon' in stats) { var icon = stats['icon']; delete stats['icon']; } else { var icon = 'unknown'; }
			if('color' in stats) { var color = stats['color']; delete stats['color']; } else { var color = '#000'; }
			if('favorite' in stats) { var favorite = true; delete stats['favorite']; } else { var favorite = false; }
			if('quickkey' in stats) { var quickkey = stats['quickkey']; delete stats['quickkey']; } else { var quickkey = false; }
			if('hidden' in stats) { var hidden = 'display: none;'; delete stats['hidden']; } else { var hidden = ''; }

			if(settings.view == 'grid')
			{
				var info	= '';
				var tags	= '';

				for(var key in stats)
				{
					info	+= '<tr><td class=\'tooltipcat\'>' + key + ':</td><td class=\'tooltipval\'>' + stats[key] + '</td></tr>';
					tags	+= ' ' + key + '="' + stats[key] + '" ';
				}

				$(	'<div class="inventoryitem_outer inventoryitem_outer_grid '+stats['type']+' item'+n+'"'+
					tags + ' icon="'+icon+'" color="'+color+'" style="'+hidden+'" '+
					'title="<table cellspacing=\'0\' cellpadding=\'0\' border=\'0\'>'+
					info+
					'</table>">'+
					'<div class="inventoryitem_inner" '+
					'style="background-image: url(images/icons_inventory/'+icon+'.png);'+
					'background-color: '+color+';">'+
					'<div class="quickkey"/>'+
					'<div class="favicon">'+
					'<img src="images/favorite.png"/>'+
					'</div></div></div>')
				.appendTo('.inventoryitems');
			}
			else // list
			{
				var info	= '';
				var tags	= '';
				var tooltip = '';

				for(var key in stats)
				{
					if(key != 'damage')
					{
						info	+=	'<td class=\'' + key + ' invlisttext\'>' + stats[key] + '</td>';
					}

					tags	+=	' ' + key + '="' + stats[key] + '" ';

					if(key == 'damage')
					{
						tooltip +=	'<table cellspacing=\'0\' cellpadding=\'0\' border=\'0\'><tr>'+
									'<td class=\'tooltipcat\'>' + key + ':</td>'+
									'<td class=\'tooltipval\'>' + stats[key] + '</td>'+
									'</tr></table>';
					}
				}

				$(	'<div class="inventoryitem_outer inventoryitem_outer_list '+stats['type']+' item'+n+'"'+
					tags + ' icon="'+icon+'" color="'+color+'" style="'+hidden+'" title="'+tooltip+'">'+
					'<table cellspacing=\'0\' cellpadding=\'0\' border=\'0\' class="inventoryitem_table">'+
					'<tr><td class="tableimage">'+
					'<div class="inventoryitem_inner inventory_inner_list" '+
					'style="background-image: url(images/icons_inventory/'+icon+'.png);'+
					'background-color: '+color+';"/>'+
					'</td>'+info+'</tr>'+
					'</table>'+
					'<div class="quickkey quickkey_list"/>'+
					'<div class="favicon favicon_list">'+
					'<img src="images/favorite.png"/>'+
					'</div></div></div>')
				.appendTo('.inventoryitems');
			}

			if(favorite)
			{
				$('.item'+n).addClass('favorite').find('.favicon').show();
			}

			if(quickkey != false)
			{
				$('.item'+n).find('.quickkey').text(quickkey).show();
			}

			UI.bindClick();
		}
	},
	drawSorting: function(settings)
	{
		$('.invsorting').append(
			'<div class="inventorysortinglist">'+
			'<table cellspacing="0" cellpadding="0" border="0" class="invsorttablelist">'+
			'<tr></tr></table></div>');

		$('.invsorting').append(
			'<div class="inventorysortinggrid">'+
			'<table cellspacing="0" cellpadding="0" border="0" class="invsorttablegrid">'+
			'<tr></tr></table></div>');

		var mwidth = 0;

		$.each(Inventory.sortList(), function(n, v)
		{
			var w = 'style="width: '+v['width']+'px;"';
			mwidth += v['width'];

			$('.invsorttablelist tr')
				.append('<td class="'+v['safe0']+' snametd">'+
						'<div>'+
						'<span class="shortname">'+v.short0+'</span>'+
						'<span class="sortArrow"></span>'+
						'</div></td>');

			$('.invsorttablegrid tr')
				.append('<td class="'+v['safe0']+' snametd" '+w+'>'+
						'<div style="text-align: left;">'+
						'<span class="longname">'+v.name0+'</span>'+
						'<span class="sortArrow"></span>'+
						'</div></td>');

			$('.'+v['safe0']+' div')
			.bind('click', function()
			{
				Inventory.drawSortArrows(true, v);
			});

			if(v.safe0 == settings.sorttype)
			{
				Inventory.drawSortArrows(false, v);
			}
		});
	},
	drawSortArrows: function(movetonext, which)
	{
		var settings = JUtils.loadSettings('inventory');
		var nextsort = {};
		var sorttype = {};
		var whicharrow = $('.'+which['safe0']+' div');
		settings['sortway'] = (typeof settings['sortway'] === 'undefined') ? 'desc' : settings['sortway'];
		sorttype = {'sorttype' : which.safe0};

		if(movetonext == false)
		{
			settings['sortway'] = Inventory.getCurrentSortWay(settings['sortway']);
		}

		if(movetonext == true)
		{
			if(settings.sorttype != which.safe0)
			{
				$('.sortArrow')
					.hide()
					.removeClass('sortUpArrow sortDownArrow')
				settings['sortway'] = 'desc';
			}
		}

		switch(settings['sortway'])
		{
			case 'desc':
				whicharrow
					.find('.sortArrow')
					.stop()
					.removeClass('sortUpArrow')
					.addClass('sortDownArrow')
					.css('width', 10)
					.show(300)
					.css('display', 'inline-block');
				nextsort = {'sortway' : 'asc'};
			break;

			case 'asc':
				whicharrow
					.find('.sortArrow')
					.stop()
					.removeClass('sortDownArrow')
					.addClass('sortUpArrow')
					.css('width', 10)
					.show(300);
				nextsort = {'sortway' : 'none'};
			break;

			case 'none':
				var sa = whicharrow.find('.sortArrow');
				var w = sa.width();
				sa
					.stop()
					.animate({'width': 0}, 200, function() {
						$(this)
							.hide()
							.removeClass('sortUpArrow')
							.css('width', w);
					});
				nextsort = {'sortway' : 'desc'};
				sorttype = {'sorttype' : Inventory.sortList()[0].safe0};
			break;
		}

		var sorting = $.extend(settings, nextsort, sorttype);
		JUtils.saveSettings('inventory', sorting);
		Inventory.updateInventory(sorting);
	},
	updateInventory: function(sorting)
	{
		if(typeof sorting === 'undefined')
		{
			var view = JUtils.loadSettings('inventory');
			if(view.view == 'grid') { var currentview = {view : 'list'}; }
			else { var currentview = {view : 'grid'}; }
			var sendstr = $.extend(view, currentview);
			JUtils.saveSettings('inventory', sendstr);

		}
		else
		{
			var sendstr = sorting;
		}

		var items = [];
		var invitouter = $('.inventoryitem_outer');

		invitouter.each(function()
		{
			var str = '{';
			var tis = $(this);

			$.each(this.attributes, function(n, v)
			{
				if(	$(v)[0].name.toLowerCase() != 'class' &&
					$(v)[0].name.toLowerCase() != 'style' &&
					$(v)[0].name.toLowerCase() != 'title')
				{
					str += '"' + $(v)[0].name + '" : "' + $(v)[0].value + '",';
				}
			});

			if(tis.hasClass('favorite')) { str += '"favorite" : "1",' }
			if(tis.is(':hidden')) { str += '"hidden" : "1",' }
			if(tis.find('.quickkey').text().length > 0) { str += '"quickkey" : "'+tis.find('.quickkey').text()+'"' }
			if(str.substring(str.length-1) == ",") { str = str.substring(0, str.length-1) }
			str += '}';
			items.push(JSON.parse(str));
		});

		if(typeof sorting !== 'undefined' && items.length > 0)
		{
			if(sorting['sortway'] == 'asc') { sorting['sortway'] = 'desc'}
			else if(sorting['sortway'] == 'none') { sorting['sortway'] = 'asc'}
			JUtils.sortJSON(items, sorting['sorttype'], sorting['sortway']);
		}

		$('.inventoryitems').html('');
		Inventory.drawItems(items, sendstr);
		UI.updateScrollbars('inventory');
	},
	updateSortingSize: function()
	{
		var settings = JUtils.loadSettings('inventory');

		if(settings.view == 'grid' || typeof settings['view'] === 'undefined')
		{
			$('.invsorttablegrid').show();
			$('.invsorttablelist').hide();
		}
		else
		{
			$('.invsorttablegrid').hide();
			$('.invsorttablelist').show();
		}
	},
	getCurrentSortWay: function(str)
	{
		if(str == 'asc')
		{
			return 'desc';
		}
		else if(str == 'desc')
		{
			return 'none';
		}
		else
		{
			return 'asc';
		}
	},
	bindScrollbarDirection: function(o)
	{
		var sb = $('.changescrollbar');
		if(o['view'] == 'list') { sb.hide(); }
		var smsg = (o['horizontal'] != true) ? 'Switch to Vertical Scrollbar' : 'Switch to Horizontal Scrollbar';
		if(o['horizontal'] == true) { sb.addClass('scrollbar_hor'); };
		sb.attr('title', smsg).bind('click', function(){Inventory.toggleScrollbarDirection(o.type, o.attachto)});
		sb.bind('mouseenter', function() { $(this).stop().animate({'opacity' : 1 }, 200).rotate({animateTo: 90, duration: 200}); });
		sb.bind('mouseleave', function() { $(this).stop().animate({'opacity' : 0.7 }, 200).rotate({animateTo: 0, duration: 200}); });
	},
	toggleScrollbarDirection: function(type, attachto)
	{
		var obj = $('#' + type + ' .changescrollbar');
		var settings = JUtils.loadSettings('inventory');

		if(settings['horizontal'] != true)
		{
			obj.addClass('scrollbar_hor');
			JUtils.changeToolTip(obj, 'Switch to Vertical Scrollbar');
			JUtils.saveSettings(type, {horizontal: true});
			UI.updateScrollbars('inventory');
		}
		else
		{
			obj.removeClass('scrollbar_hor');
			JUtils.changeToolTip(obj, 'Switch to Horizontal Scrollbar');
			JUtils.saveSettings(type, {horizontal: false});
			UI.updateScrollbars('inventory');
		}
	},
	filterContent: function(o)
	{
		var ft = $('.filterinventory');

		$('<input type="text" placeholder="Filter Inventory"/>')
		.bind('mouseenter', function() { $(this).stop().animate({'opacity' : 1 }, 200); })
		.bind('mouseleave', function() { if(!$(this).is(':focus')) { $(this).stop().animate({'opacity' : 0.8 }, 200); }})
		.focus(function(e)
		{
			e.preventDefault();
			$(o.attachto).unbind('keydown');
			$(this).stop().animate({'opacity' : 1 }, 200);
		})
		.blur(function()
		{
			UI.bindKeyPress(o);
			$(this).stop().animate({'opacity' : 0.8 }, 200);
		})
		.bind('keyup', function()
		{
			var v = $(this).val().toLowerCase();
			$.each($('.inventoryitem_outer'), function()
			{
				var name = $(this).attr('name').toLowerCase();
				if(name.indexOf(v) === -1) { $(this).hide(200); }
				else { $(this).show(200); }
			});
		})
		.appendTo(ft);
	},
	sortList: function()
	{
		return [{	'name0'  : 'Name',
					'safe0'  : 'name',
					'short0' : 'Name',
					'width'  : 74
				},
				{	'name0'  : 'Type',
					'safe0'  : 'type',
					'short0' : 'Type',
					'width'  : 70
				},
				{	'name0'  : 'Material',
					'safe0'  : 'material',
					'short0' : 'Mat',
					'width'  : 95
				},
				{	'name0'  : 'Weight',
					'safe0'  : 'weight',
					'short0' : 'Wgt',
					'width'  : 86
				},
				{	'name0'  : 'Value',
					'safe0'  : 'value',
					'short0' : 'Val',
					'width'  : 75
				}
		];
	},
	populateInventory: function()
	{
		var content = [];
		// Random Weapons, Armors, Prefixes and Suffixes
		var numOfWeapons	= JUtils.ranNum(15, 20);
		var numOfArmors		= JUtils.ranNum(15, 20);
		var prefixes		= DB.prefixes();
		var suffixes		= DB.suffixes();

		for(var i = 0 ; i < numOfWeapons ; i++)
		{
			var weapon		= JUtils.ranKey(DB.items().weapon);
			var weaponicon	= weapon.replace(/[^a-zA-Z]+/g, '').toLowerCase();
			var material	= JUtils.ranKey(DB.items().weapon[weapon][0]);
			var prefix		= prefixes[Math.round(Math.random()*(prefixes.length-1))];
			var suffix		= suffixes[Math.round(Math.random()*(suffixes.length-1))];

			content.push(
			{
				'name'		: prefix + ' ' + weapon + ' ' + suffix,
				'damage'	: Calc.weaponDamage(weapon, material),
				'type'		: 'weapon',
				'material'	: material,
				'weight'	: Calc.weaponWeight(weapon, material),
				'value'		: Calc.weaponValue(weapon, material),
				'icon'		: weaponicon,
				'color'		: DB.material()[material][4]
			});
		}

		for(var i = 0 ; i < numOfArmors ; i++)
		{
			var armor		= JUtils.ranKey(DB.items().armor);
			var armoricon	= armor.replace(/[^a-zA-Z]+/g, '').toLowerCase();
			var material	= JUtils.ranKey(DB.items().armor[armor][0]);
			var prefix		= prefixes[Math.round(Math.random()*(prefixes.length-1))];
			var suffix		= suffixes[Math.round(Math.random()*(suffixes.length-1))];

			content.push(
			{	'name'		: prefix + ' ' + armor + ' ' + suffix,
				'type'		: 'armor',
				'material'	: material,
				'weight'	: Calc.armorWeight(armor, material),
				'value'		: Calc.armorValue(armor, material),
				'icon'		: armoricon,
				'color'		: DB.material()[material][4]
			});
		}

		return content;
	}
};