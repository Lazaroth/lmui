Settings = {
	create: function(o)
	{
		var render3d = '';

		if(o.showworld == true)
		{
			render3d =	'<li class="togglebackground">'+
							'<a>'+
								'<div class="settingscbtext">Render 3D World</div>'+
								'<div class="checkbox">'+
									'<input type="checkbox" id="checkboxInput2"/>'+
									'<label for="checkboxInput2"/>'+
								'</div>'+
							'</a>'+
						'</li>';
		}

		$(o.attachto).append(
			'<div id="sidr">'+
				'<h1>Workspace</h1>'+
				'<ul>'+
					'<li>'+
						'<select id="workspace"/>'+
					'</li>'+
				'</ul>'+
				'<h1>Theme</h1>'+
				'<ul>'+
					'<li>'+
						'<select id="themes"/>'+
					'</li>'+
				'</ul>'+
				'<h1>Scene</h1>'+
				'<ul>'+
					'<li>'+
						'<select id="scenes">'+
							'<option value="night">Night</option>'+
							'<option value="rain">Rain</option>'+
							'<option value="snow">Snow</option>'+
						'</select>'+
					'</li>'+
				'</ul>'+
				'<h1>Settings</h1>'+
				'<ul>'+
					'<li class="uiscaleslider">'+
						'<a>'+
							'<div id="uiscaletext">UI Scale</div>'+
							'<div id="uiscalenumber"/>'+
							'<div class="uiscalesliderarm"/>'+
						'</a>'+
					'</li>'+
					'<li class="marginscaleslider">'+
						'<a>'+
							'<div id="marginscaletext">Margin Size</div>'+
							'<div id="marginscalenumber"/>'+
							'<div class="marginscalesliderarm"/>'+
						'</a>'+
					'</li>'+
					'<li class="toggleeffects">'+
						'<a>'+
							'<div class="settingscbtext">Inventory Effects</div>'+
							'<div class="checkbox">'+
								'<input type="checkbox" id="checkboxInput1" />'+
								'<label for="checkboxInput1"/>'+
							'</div>'+
						'</a>'+
					'</li>'+
					render3d +
					'<li class="resetsettings">'+
						'<a>'+
							'Reset Settings'+
							'<div>Refreshing...</div>'+
						'</a>'+
					'</li>'+
				'</ul>'+
			'</div>'
		);

		Settings.bindLiToCheckbox();
		Settings.createSettingsButton(o);
		Settings.switchWorkspace(o);
		Settings.moveWorkspaceWindow(o);
		Settings.switchTheme(o);
		Settings.switchScene(o);
		Settings.scaleUI(o);
		Settings.marginSize(o);
		Settings.inventoryEffects(o);
		Settings.resetSettings(o);
		Settings.adjustDropdowns();
		Changelog.create(o);
		if(o.showworld == true) { Settings.renderWorld(o); }
	},
	createSettingsButton: function(o)
	{
		$('<a id="settingsButton" href="#sidr"></a>')
			.bind('mouseenter', function() { $(this).stop().animate({'opacity' : 1 }, 200); })
			.bind('mouseleave', function() { $(this).stop().animate({'opacity' : 0.5 }, 200); })
			.bind('click', function()
			{
				Settings.openSettings(o);
			})
			.sidr({	side: 'right', displace: false, body: o.attachto})
			.appendTo(o.attachto);
	},
	openSettings: function(o)
	{
		if($('.sidranim').length == 0)
		{
			$(o.attachto).append('<div class="sidranim"/>');

			$('.moveWorkspace')
				.stop()
				.fadeToggle(200, function()
				{
					$('.sidranim').remove();
				});
		}
	},
	switchWorkspace: function(o)
	{
		for(var i = 0 ; i < Defaults.Workspaces ; i++)
		{
			var name = (typeof o['workspaces'][i] != 'undefined') ? o['workspaces'][i]['name'] : 'Unused';
			$('#workspace').append('<option value="'+(i+1)+'">'+(i+1)+': '+name+'</option>');
		};

		$('#workspace option[value="'+o.theme+'"]').attr("selected","selected");

		$('#workspace').easyDropDown(
		{
			cutOff: 5,
			onChange: function(selected)
			{
				UI.toggleWorkSpace(parseInt(selected.value)-1, true);
				Settings.updateWorkspaceInputs(selected.value);
			}
		});

		$('#workspace')
			.closest('li')
			.after('<li id="workspacebutton"/><li id="workspacename"/>');

		$('#workspacebutton')
			.append('<a class="settingswsa"><div class="settingswstext">Button</div><input type="text" placeholder="Press a Key" key="'+o['workspaces'][0]['button']+'" class="settingsinput"/></a>');
		$('#workspacename')
			.append('<a class="settingswsa"><div class="settingswstext">Name</div><input type="text" placeholder="No Name" class="settingsinput"/></a>');

		$('.settingsinput')
			.closest('li')
			.hover(function()
			{
				$(this).css('background', 'transparent');
			})
			.css(
			{
				'border-top' : 'none',
				'border-bottom' : '1px solid #4d4d4d'
			});

		Settings.updateWorkspaceInputs('1');
		Settings.bindWorkspaceInputs(o);
	},
	updateWorkspaceInputs: function(workspace)
	{
		var settings = JUtils.loadSettings('settings');

		var code = settings['workspaces'][parseInt(workspace)-1]['button'];
		var name = settings['workspaces'][parseInt(workspace)-1]['name'];

		if(code != -1)
		{
			$('#workspacebutton .settingsinput')
				.attr('key', code)
				.val(JUtils.getKey(code));
		}
		else
		{
			$('#workspacebutton .settingsinput')
				.attr('key', -1)
				.val('No key set');
		}

		if(name != 'No Name')
		{
			$('#workspacename .settingsinput')
			.val(name);
		}
		else
		{
			$('#workspacename .settingsinput')
			.val('');
		}
	},
	bindWorkspaceInputs: function(o)
	{
		$('#workspacebutton .settingsinput')
			.bind('keydown', function(e)
			{
				e.preventDefault();
				$(o.attachto).unbind('keydown');
			})
			.bind('keyup', function(e)
			{
				e.preventDefault();
				var keyCode = e.keyCode || e.which;
				var keyName = JUtils.getKey(keyCode);

				if(keyName !== false)
				{
					var workspace = $('#workspace').val();
					e.preventDefault();
					var settings = JUtils.loadSettings('settings');
					settings['workspaces'][parseInt(workspace)-1]['button'] = keyCode;
					JUtils.saveSettings('settings', settings);
					$(this)
						.attr('key', keyCode)
						.val(keyName)
						.blur();
				}
				else
				{
					JUtils.popup($(this),
								'Invalid Button',
								true,
								{
									'y' : -35
								});
				}
			})
			.focus(function()
			{
				Defaults.World.paused = true;
				$(this)
					.addClass('settingsinputfocus')
					.val('');
			})
			.blur(function()
			{
				Defaults.World.paused = false;
				var o = JUtils.loadSettings('settings');
				var workspace = $('#workspace').val();
				var key = JUtils.getKey(o['workspaces'][parseInt(workspace)-1]['button']);
				$(o.attachto).unbind('keydown');
				UI.bindKeyPress(o);

				if($(this).val() != '')
				{
					$(this)
					.removeClass('settingsinputfocus')
					.val(key);
				}
				else
				{
					if(parseInt($(this).attr('key')) == -1)
					{
						$(this)
						.removeClass('settingsinputfocus')
						.val('No key set');
					}
					else
					{
						$(this)
						.removeClass('settingsinputfocus')
						.val(JUtils.getKey(parseInt($(this).attr('key'))));
					}
				}
			});

		$('#workspacename .settingsinput')
			.focus(function()
			{
				$(o.attachto).unbind('keydown');
			})
			.blur(function()
			{
				if($.trim($(this).val()) != '')
				{
					var o = JUtils.loadSettings('settings');
					var workspace = $('#workspace').val();
					o['workspaces'][parseInt(workspace)-1]['name'] = $(this).val();
					JUtils.saveSettings('settings', o);
					Settings.updateWorkspaceSelect(workspace, $(this).val());
				}

				UI.bindKeyPress(o);
			});
	},
	moveWorkspaceWindow: function(o)
	{
		var height = ($(o.attachto).height()-10);
		var oldnum = 0;
		var ttop   = 0;

		for(var i = 0 ; i < Defaults.Workspaces ; i++)
		{
			var num = Math.floor(i*40/height);

			if(oldnum != num)
			{
				ttop = 0;
				oldnum = num;
			}

			$('<div class="moveWorkspace" workspace="'+i+'">'+
			  '<div class="moveWorkspace_inner">'+(i+1)+'</div>'+
			  '</div>')
				.css('top', ttop*40)
				.css('left', num*40)
				.bind('mousedown', function()
				{
					var ws = parseInt($(this).attr('workspace'));
					UI.toggleWorkSpace(ws, true);
					Settings.updateWorkspaceSelect(ws+1);
					Settings.updateWorkspaceInputs(ws+1);
				})
				.appendTo(o.attachto);

			if(oldnum == num)
			{
				ttop++;
			}
		}
	},
	switchTheme: function(o)
	{
		$.each(themeList, function(n, v)
		{
			$('#themes').append('<option value="'+v['folder']+'">'+v['name']+'</option>');
		});

		$('#themes option[value="'+o.theme+'"]').attr("selected","selected");

		$('#themes').easyDropDown(
		{
			cutOff: 5,
			onChange: function(selected)
			{
				if(UI.isPaused())
				{
					$('.uiWindow')
						.fadeOut(o.fxtimer)
						.promise()
						.done(function()
						{
							Themes.loadTheme(selected.value, true, function(settings)
							{
								$('#workspace')
									.easyDropDown('destroy')
									.empty();
								$('#workspacebutton, #workspacename').remove();
								Settings.switchWorkspace(settings);
								$('.uiWindow').remove();
								Themes.loadStyle(selected.value, function()
								{
									UI.createWindows();
									UI.updateScrollbars();
									UI.toggleWorkSpace(0, true);
								});
							});
						});
				}
				else
				{
					Themes.selectTheme(selected.value, true, function(settings)
					{
						$('#workspace')
							.easyDropDown('destroy')
							.empty();
						$('#workspacebutton, #workspacename').remove();
						Settings.switchWorkspace(settings);
						$('.uiWindow').remove();
						Themes.loadStyle(selected.value, function()
						{
							UI.createWindows();
							UI.updateScrollbars();
							if(settings['sticky'].length > 0)
							{
								$.each(settings['sticky'], function(n, windowname)
								{
									$('#'+windowname).show();
								});
							}
						});
					});
				}
			}
		});
	},
	switchScene: function(o)
	{
		$('#scenes option[value="'+o.scene+'"]').attr("selected","selected");

		$('#scenes').easyDropDown(
		{
			cutOff: 5,
			onChange: function(selected)
			{
				var o = JUtils.loadSettings('settings');

				if(o.scene != selected.value)
				{
					Defaults.World.current = selected.value;
					if(UI.isPaused() && o.fxshow) { FX.hide(o.scene); }
					if(o.world == true)
					{
						World.stop(o);
						setTimeout(function() // wait for stop
						{
							World.start(o);
						}, 30);
					}
					else
					{
						UI.removeBackground(o);
						UI.createBackground($.extend(o, {scene: selected.value}));
					}
					if(UI.isPaused() && o.fxshow) { FX.show(selected.value); }
					var val = {scene: selected.value};
					JUtils.saveSettings('settings', val);
				}
			}
		});
	},
	bindLiToCheckbox: function()
	{
		$('#sidr')
			.find('input[type=checkbox]')
			.closest('li')
			.bind('click', function()
			{
				$(this).find('input[type=checkbox]').click();
			});

		$('#sidr')
			.find('label')
			.bind('click', function()
			{
				$(this).closest('li').find('input[type=checkbox]').click();
				return false;
			});
	},
	adjustDropdowns: function()
	{
		$('#sidr select')
			.closest('li')
			.css('border', 'none')
			.css('line-height', '16px')
			.hover(function(){ $(this).css("background","transparent"); })
			.closest('ul')
			.css('border', 'none');
	},
	scaleUI: function(o)
	{
		$(".uiscalesliderarm")
			.slider(
			{
				value:	o.scale,
				min:	50,
				max:	200,
				step:	10,
				slide:	function(event, ui)
				{
					$("#uiscalenumber").html(ui.value + '%');
				},
				stop:	function(event, ui)
				{
					var oldsettings = JUtils.loadSettings('settings');
					UI.rescaleUI(ui.value, oldsettings.scale);
					JUtils.saveSettings('settings', {'scale' : ui.value});
					UI.updateScrollbars();
				}
			})
			.css({	'width'			: '90px',
					'display'		: 'inline-block'
			})
			.find('.ui-slider-handle')
			.unbind('keydown');

		$("#uiscaletext")
			.css({	'display'		: 'inline-block',
					'width'			: '95px'
			});

		$("#uiscalenumber")
			.html(o.scale + '%')
			.css({	'display'		: 'inline-block',
					'width'			: '40px',
					'font-size'		: '0.7em'
			});

		$(".uiscalesliderarm .ui-state-default")
			.css({	'background'	: '#ddd',
					'width'			: '13px',
					'padding'		: '0px',
					'border-radius'	: '8px',
					'z-index'		: '0'
			});
	},
	marginSize: function(o)
	{
		$(".marginscalesliderarm")
			.slider(
			{
				value:	o.margins,
				min:	10,
				max:	200,
				step:	10,
				slide:	function(event, ui)
				{
					$("#marginscalenumber").html(ui.value + '%');
				},
				stop:	function(event, ui)
				{
					var oldsettings = JUtils.loadSettings('settings');
					UI.resizeMargins(ui.value, oldsettings.margins);
					JUtils.saveSettings('settings', {'margins' : ui.value});
					UI.updateScrollbars();
				}
			})
			.css({	'width'			: '90px',
					'display'		: 'inline-block'
			})
			.find('.ui-slider-handle')
			.unbind('keydown');

		$("#marginscaletext")
			.css({	'display'		: 'inline-block',
					'width'			: '95px'
			});

		$("#marginscalenumber")
			.html(o.margins + '%')
			.css({	'display'		: 'inline-block',
					'width'			: '40px',
					'font-size'		: '0.7em'
			});

		$(".marginscalesliderarm .ui-state-default")
			.css({	'background'	: '#ddd',
					'width'			: '13px',
					'padding'		: '0px',
					'border-radius'	: '8px',
					'z-index'		: '0'
			});
	},
	inventoryEffects: function(o)
	{
		$('#checkboxInput1')
			.prop('checked', o.fxshow)
			.bind('click', function()
			{
				var settings = JUtils.loadSettings('settings');

				if($(this).is(':checked'))
				{
					JUtils.saveSettings('settings', {fxshow: true});
					if(UI.isPaused())
					{
						FX.show(settings.scene);
					}
				}
				else
				{
					JUtils.saveSettings('settings', {fxshow: false});
					if(UI.isPaused())
					{
						FX.hide(settings.scene);
					}
				}
			});
	},
	renderWorld: function(o)
	{
		$('#checkboxInput2')
			.prop('checked', o.world)
			.bind('click', function()
			{
				var settings = JUtils.loadSettings('settings');
				if(UI.isPaused() && settings.fxshow) { FX.hide(settings.scene); }

				if($(this).is(':checked'))
				{
					JUtils.saveSettings('settings', {world: true});

					if(settings.showworld)
					{
						UI.removeBackground(settings);
						World.start();
					}
				}
				else
				{
					JUtils.saveSettings('settings', {world: false});

					if(settings.showworld)
					{
						World.stop();
						UI.createBackground(settings);
					}
				}

				if(UI.isPaused() && settings.fxshow) { FX.show(settings.scene); }
			});
	},
	resetSettings: function(o)
	{
		$('.resetsettings').bind('click', function()
		{
			for(var cookie in $.cookie())
			{
			  $.removeCookie(cookie);
			}

			$('.resetsettings > a > div').fadeIn(200, function()
			{
				location.reload();
			});
		});
	},
	updateWorkspaceSelect: function(num, text)
	{
		$('#workspace').easyDropDown('destroy');

		$('#workspace option[value="'+num+'"]')
			.attr("selected","selected");

		if(typeof text != 'undefined')
		{
			$('#workspace option[value="'+num+'"]')
				.text(num + ': ' + text);
		}

		$('#workspace').easyDropDown(
		{
			cutOff: 5,
			onChange: function(selected)
			{
				UI.toggleWorkSpace(parseInt(selected.value)-1, true);
				Settings.updateWorkspaceInputs(selected.value);
			}
		});
	},
};