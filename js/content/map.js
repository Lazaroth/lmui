Map = {
	create: function(content, type)
	{
		var s = JUtils.loadSettings('settings');
		var scale = s.scale / 100;

		var opacity = $('#map').css('opacity');
		$('#map').css('opacity', 0).show();

		$('#map .content').append('<div id="olmap"><div id="mappopup"/></div>');

		var locations = DB.locations().length;

		for(var i = 0 ; i < locations ; i++)
		{
			Map.createLocations(i, scale);
		}

		var pixelProjection = new ol.proj.Projection(
		{
			code: 'pixel',
			units: 'pixels',
			extent: [0, 0, Defaults.mapinfo[0], Defaults.mapinfo[1]]
		});

		var maplayers = [	new ol.layer.Image({
								source: new ol.source.ImageStatic(
								{
									url: Defaults.mapinfo[2],
									imageSize: [Defaults.mapinfo[0], Defaults.mapinfo[1]],
									projection: pixelProjection,
									imageExtent: pixelProjection.getExtent()
								})
							})
						];

		for(var key in Defaults.mapLayerTypes)
		{
			var mapsource = new ol.source.Vector({features: Defaults.mapLayerTypes[key].features});
			var maplayer = new ol.layer.Vector({source: mapsource, maxResolution: 6});
			if($.inArray(key, s.mapfilter) != -1) { maplayer.setVisible(false); }
			maplayers.push(maplayer);
		}

		MAP = new ol.Map(
		{
			layers: maplayers,
			controls: [],
			target: 'olmap',
			view: new ol.View2D(
			{
				projection: pixelProjection,
				maxZoom: 6
			})
		});

		Map.createPopup();
		Map.searchMap(s);
		Map.filterLayers(s);

		$('#map').css('opacity', opacity).hide();
		$('#olmap').bind('click', function() { $('#searchmap:focus').blur(); });
	},
	createLocations: function(num, s)
	{
		var type = DB.locations()[num].t;

		var loc = new ol.Feature(
		{
			geometry: new ol.geom.Point([DB.locations()[num].x, Defaults.mapinfo[1]-DB.locations()[num].y]),
			name: DB.locations()[num].n
		});

		loc.setStyle(Map.getIcon(type, s));
		Defaults.mapLayerTypes[type]['features'].push(loc);
	},
	getIcon: function(type, s)
	{
		var icon = new ol.style.Style(
		{
			image: new ol.style.Icon(
			{
				anchor: [0.5, 30],
				anchorXUnits: 'fraction',
				anchorYUnits: 'pixels',
				opacity: 1,
				rotateWithView: true,
				src: 'images/icons_map/'+Defaults.mapLayerTypes[type]['icon1'],
				scale: 0.7 * s
			})
		});

		return icon;
	},
	createPopup: function()
	{
		var element = document.getElementById('mappopup');

		MAPPOPUP = new ol.Overlay(
		{
			element: element,
			positioning: 'bottom-center',
			stopEvent: false
		});

		MAP.addOverlay(MAPPOPUP);

		MAP.on('pointerdrag', function(e)
		{
			JUtils.deletePopup(element);
		});

		MAP.on('mousewheel', function(e)
		{
			JUtils.deletePopup(element);
		});

		MAP.on('click', function(e)
		{
			var feature = MAP.forEachFeatureAtPixel(e.pixel, function(feature, layer)
			{
				return feature;
			});

			if(feature)
			{
				var name		= feature.get('name');
				var geometry	= feature.getGeometry();
				var coord		= geometry.getCoordinates();
				MAPPOPUP.setPosition(coord);
				Map.popup(element, name);
			}
			else
			{
				JUtils.deletePopup(element);
			}
		});
	},
	popup: function(element, txt)
	{
		JUtils.popup(	element,
						txt,
						false,
						{
							'y'		: -50,
							'cax'	: true
						},
						true);
	},
	resizeMap: function()
	{
		var map			= $('#map');
		var ishidden	= map.is(':hidden');
		var opacity		= map.css('opacity');
		var viewport	= $('#map .content');

		if(ishidden) { map.css('opacity', 0).show(); }

		var h			= viewport.height();
		var w			= viewport.width();

		$('#olmap')
			.height(h)
			.width(w);
		MAP.updateSize();
		var view = MAP.getView();

		// zoom 0 = 256x128, zoom 1 = 512x256 etc.
		if(w > 16384)
		{
			view.setZoom(6);
		}
		else if(w > 8192)
		{
			view.setZoom(5 + (w/8192)-1);
		}
		else if(w > 4096)
		{
			view.setZoom(4 + (w/4096)-1);
		}
		else if(w > 2048)
		{
			view.setZoom(3 + (w/2048)-1);
		}
		else if(w > 1024)
		{
			view.setZoom(2 + (w/1024)-1);
		}
		else if(w > 512)
		{
			view.setZoom(1 + (w/512)-1);
		}
		else if(w > 256)
		{
			view.setZoom(0 + (w/256)-1);
		}
		else
		{
			view.setZoom(0);
		}

		view.setCenter([Defaults.mapinfo[0]/2, Defaults.mapinfo[1]/2]);
		if(ishidden) { map.css('opacity', opacity).hide(); }
	},
	searchMap: function(o)
	{
		var maplist = [];
		var mlen = DB.locations().length;

		for(var i = 0 ; i < mlen ; i++)
		{
			maplist.push({
				label: DB.locations()[i].n,
				value: {
					x: DB.locations()[i].x,
					y: DB.locations()[i].y
				}
			});
		}

		$(	'<div class="ui-widget">'+
			'<input type="text" id="searchmap" placeholder="Find Location"/>'+
			'</div>')
		.appendTo('.searchmap');

		$('#searchmap')
		.bind('mouseenter', function() { $(this).stop().animate({'opacity' : 1 }, 200); })
		.bind('mouseleave', function() { if(!$(this).is(':focus')) { $(this).stop().animate({'opacity' : 0.8 }, 200); }})
		.focus(function(e)
		{
			e.preventDefault();
			$(o.attachto).unbind('keydown');
			$(this).stop().animate({'opacity' : 1 }, 200);
		})
		.blur(function()
		{
			$(o.attachto).unbind('keydown');
			UI.bindKeyPress(o);
			$(this).stop().animate({'opacity' : 0.8 }, 200);
		})
		.autocomplete({
			source: maplist,
			select: function(e, ui)
			{
				e.preventDefault();
				JUtils.deletePopup('#mappopup');

				var view = MAP.getView();
				var start = +new Date();
				var usebounce = (view.getResolution() < 7) ? true : false;
				if(usebounce) { var duration = 1000; }
				else { var duration = 500; }

				var bounce = ol.animation.bounce({
					duration: duration,
					resolution: 8,
					start: start
				});

				var pan = ol.animation.pan({
					duration: duration,
					source: view.getCenter(),
					start: start
				});

				var zoom = ol.animation.zoom({
					duration: duration,
					resolution: view.getResolution(),
					start: start
				});

				var rotate = ol.animation.rotate(
				{
					duration: duration-30,
					rotation: view.getRotation() + (Math.PI / 32),
					start: start+30
				});

				if(usebounce)	{ MAP.beforeRender(rotate, pan, zoom, bounce); }
				else			{ MAP.beforeRender(pan, zoom); }
				view.setZoom(4);
				view.setRotation(0);
				view.setCenter([ui.item.value.x, Defaults.mapinfo[1]-ui.item.value.y]);
				$('#searchmap').val('').blur();

				// show popup after animation is finished
				setTimeout(function()
				{
					MAPPOPUP.setPosition([ui.item.value.x, Defaults.mapinfo[1]-ui.item.value.y]);
					var element = document.getElementById('mappopup');
					Map.popup(element, ui.item.label);
				}, duration);
			},
			focus: function(e, ui)
			{
				e.preventDefault();
			}
		});
	},
	filterLayers: function(o)
	{
		var n = 1;

		for(var key in Defaults.mapLayerTypes)
		{
			var visible	= (Defaults.mapLayerTypes[key]['visible'] && $.inArray(key, o.mapfilter) == -1) ? true : false;
			var icon	= (visible) ? Defaults.mapLayerTypes[key]['icon1'] : Defaults.mapLayerTypes[key]['icon2'];
			var shtitle	= (visible) ? 'Hide ' : 'Show ';
			var titfull	= shtitle + JUtils.capFirst(key) + 's';

			$('<img src="images/icons_map/'+icon+'" id="mapfilter'+key+'" title="'+titfull+'"/>')
			.appendTo('.mapfilters');

			Map.showHideLayers($('#mapfilter'+key), n, key);
			n++;
		}
	},
	showHideLayers: function(input, num, key)
	{
		input.bind('click', function(e)
		{
			var s = JUtils.loadSettings('settings');
			JUtils.deletePopup('#mappopup');

			if($.inArray(key, s.mapfilter) != -1)
			{
				MAP.getLayers().forEach(function(f, n)
				{
					if(n == num)
					{
						JUtils.changeToolTip(input, 'Hide ' + JUtils.capFirst(key) + 's');
						input.attr('src', 'images/icons_map/'+Defaults.mapLayerTypes[key]['icon1']);

						for(var i = (s.mapfilter.length-1) ; i > -1 ; i--)
						{
							if(s.mapfilter[i] == key)
							{
								s.mapfilter.splice(i, 1);
							}
						}

						JUtils.saveSettings('settings', {'mapfilter': s.mapfilter})
						f.setVisible(true);
						return false;
					}
				});
			}
			else
			{
				MAP.getLayers().forEach(function(f, n)
				{
					if(n == num)
					{
						JUtils.changeToolTip(input, 'Show ' + JUtils.capFirst(key) + 's');
						input.attr('src', 'images/icons_map/'+Defaults.mapLayerTypes[key]['icon2']);
						var exists = false;

						for(var i = 0 ; i < s.mapfilter.length ; i++)
						{
							if(s.mapfilter[i] == key)
							{
								exists = true;
								return false;
							}
						}

						if(!exists)
						{
							s.mapfilter.push(key);
						}

						JUtils.saveSettings('settings', {'mapfilter': s.mapfilter});
						f.setVisible(false);
						return false;
					}
				});
			}
		});
	}
}