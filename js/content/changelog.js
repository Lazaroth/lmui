Changelog = {
	create: function(o)
	{
		$('#sidr').prepend('<h1 id="showchangelog">LMUI v'+LMUIVersion+'</h1>');

		$('#showchangelog').bind('click', function()
		{
			if($('#changelog').length == 0)
			{
				var c = 'changelog';

				$(o.attachto).drawWindow($.extend(UI.normWHXY(Defaults.WindowSettings()['changelog']), {"type": c}), true);

				$(UI.header(c)).html('Changelog');

				JUtils.getVersion(true, function(xml)
				{
					$.each($(xml).find('version'), function(n1, version)
					{
						var num = $(version).attr('number');
						$(UI.content(c)).append('<div class="changelogversion">'+num+'</div>');

						$.each($($(xml).find('version')[n1]).children(), function(n2, changes)
						{
							var cat = $(changes).prop("tagName");
							$(UI.content(c)).append('<div class="changelogcat">'+cat+'</div>');

							var basetext  = $(changes).prop("textContent");
							var cleantext = $.trim(basetext).replace(/[\t]+/g,'');
							var textarray = cleantext.split("\n");

							var ul = $('<ul/>').appendTo(UI.content(c));
							$.each(textarray, function(n3, change)
							{
								ul.append('<li><div class="changelogtext">'+change+'</div></li>');
							});
						});
					});

					$('#changelog').find('.sticky').hide();
					$('#changelog').fadeIn(o.fxtimer);
					UI.updateScrollbars('changelog');
				});
			}
			else
			{
				$('#changelog').fadeOut(o.fxtimer, function()
				{
					$(this).remove();
				});
			}
		});
	}
}