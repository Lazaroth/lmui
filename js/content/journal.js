Journal = {
	create: function(att)
	{
		$(	'<table border="0" cellspacing="0" cellpadding="0">'+
			'<tr><td valign="top">'+
			'<div id="journaltext"/>'+
			'</td><td valign="top">'+
			'<div id="journalquests"/>'+
			'</td></tr>'+
			'</table>'
		).appendTo(att);

		Journal.getQuests(function()
		{
			UI.updateScrollbars('journal');
		});
	},
	addQuests: function(quests)
	{
		$.each(quests, function(n1, v)
		{
			var sanefaction = v['faction'].replace(/\W/g, '');

			$('<div class="'+sanefaction+' journalquesttitle">'+v['faction']+'</div>')
				.appendTo('#journalquests');

			$.each(v['quests'].reverse(), function(n2, q)
			{
				var saneid = JSON.parse(q['id']).replace(/\W/g, '');
				var completed = (JSON.parse(q['completed']) == 1) ? 'completedquest' : '';
				var questtexts = q['text'].reverse();

				$('<div class="'+saneid + ' ' + completed +' journalquestsubtitle">'+JSON.parse(q['title'])+'</div>')
					.bind('click', function()
					{
						$('#journaltext').html('');
						var len = q['text'].length;

						$.each(questtexts, function(n3, t)
						{
							$('#journaltext').append('<div class="journaldate">'+JSON.parse(q['date'+(len-n3)])+'</div>');
							$('#journaltext').append('<div class="journalparagraph">'+JSON.parse(t)+'</div>');
						});

						$('.journalquestsubtitle').removeClass('currentquest');
						$(this).addClass('currentquest');
						UI.updateScrollbars('journal');
					})
					.appendTo('.'+sanefaction);
			});
		});
	},
	getQuests: function(callback)
	{
		$.ajax(
		{
			type: "GET",
			url: "quests/quests.xml",
			dataType: "xml",
			success: function(xml)
			{
				var allquests = [];

				$.each($(xml).find('quests').children(), function(n1, questlines)
				{
					var faction = $($(questlines)[0]).attr('name');
					var questline = {'faction': faction, 'quests' : []};

					$.each($(questlines).children(), function(n2, quests)
					{
						var texts = [];
						var quest = {};

						$.each($(quests).children(), function(n3, questinfo)
						{
							var tag = $(questinfo).prop('tagName');

							if(tag.replace(/[0-9]/g, '') == 'text')
							{
								var basetext	= $.trim($(questinfo).text());
								var cleantext	= basetext.replace(/[\t\n]+/g,' ');
								var playername	= cleantext.replace(/{player}/g, DB.player()['name']);
								var newline		= playername.replace(/{br}/g, '<br />');
								texts.push(JSON.stringify(newline));
							}
							else
							{
								quest[tag] = JSON.stringify($.trim($(questinfo).text()));
							}
						});

						quest['text'] = texts;
						questline['quests'].push(quest);
					});

					allquests.push(questline);
				});
				Journal.addQuests(allquests);
				if(typeof(callback) === 'function') { callback.call(this); }
			},
			error: function(xhr, str, err)
			{
				var error = [{	'faction'	: 'Error',
								'quests'	: [{'completed' : JSON.stringify('0'),
												'date1' : JSON.stringify(''),
												'id' : JSON.stringify('journalerror'),
												'title' : JSON.stringify('Journal not loaded'),
												'text' : [JSON.stringify('Unable to load quests.xml<br/>'+err)]
											  }]
							}]

				Journal.addQuests(error);
				if(typeof(callback) === 'function') { callback.call(this); }
			}
		});
	}
};